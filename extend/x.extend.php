<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

define('X_EVENT_INIT', 'INIT');
define('X_EVENT_START', 'START');
define('X_EVENT_MODULE_LOADED', 'MODULE_LOADED');
define('X_EVENT_CLOSE', 'CLOSE');
define('X_EVENT_END', 'END');

require_once G5_PATH . '/x/index.php';
if (_X_STANDALONE_) return;

ExtendX::init();

class ExtendX
{
    var $listeners = array();
    var $isInitialize = false;
    var $isStarted = false;
    var $startOutputLevel = 0;

    function &getInstance()
    {
        static $oInstance = null;
        if (!$oInstance) $oInstance = new ExtendX();
        return $oInstance;
    }

    function init()
    {
        is_a($this, 'ExtendX') ? $self =& $this : $self =& ExtendX::getInstance();
        $self->_init();
    }

    function start()
    {
        is_a($this, 'ExtendX') ? $self =& $this : $self =& ExtendX::getInstance();
        $self->_start();
    }

    function close()
    {
        is_a($this, 'ExtendX') ? $self =& $this : $self =& ExtendX::getInstance();
        $self->_close();
    }

    function transModule($buffer)
    {
        is_a($this, 'ExtendX') ? $self =& $this : $self =& ExtendX::getInstance();
        return $self->_transModuleProcess($buffer);
    }

    function on($events, $listener, $priority = 0, $once = false)
    {
        is_a($this, 'ExtendX') ? $self =& $this : $self =& ExtendX::getInstance();

        foreach ((array)$events as $event) {
            $obj = new stdClass();
            $obj->listener = $listener;
            $obj->once = $once;
            $self->listeners[$priority][$event][] = $obj;
        }
        ksort($self->listeners[$priority]);
    }

    function once($events, $func, $priority = 0)
    {
        is_a($this, 'ExtendX') ? $self =& $this : $self =& ExtendX::getInstance();

        $self->on($events, $func, $priority, true);
    }

    function emit($event)
    {
        is_a($this, 'ExtendX') ? $self =& $this : $self =& ExtendX::getInstance();

        $args = func_get_args();
        array_shift($args);

        $bufferEvents = array(X_EVENT_CLOSE, X_EVENT_MODULE_LOADED, X_EVENT_END);
        $isBufferEvent = in_array($event, $bufferEvents);
        $buffer = $isBufferEvent ? $args[0] : null;

        $previousEvent = null;

        $isStandalone = $self->_isStandalone($event);
        foreach ($self->listeners as $priority => $listeners) {
            foreach ($listeners as $eventName => $eventListeners) {
                $eventData = new stdClass();
                $eventData->type = $event;
                $eventData->standalone = $isStandalone;
                $eventData->name = $eventName;
                $eventData->priority = $priority;
                $eventData->previous_event = $previousEvent;

                if (preg_match(sprintf('@^%s$@', str_replace(array('.', '*'), array('\.', '.*'), addslashes($eventName))), $event)) {
                    foreach ($eventListeners as $idx => $obj) {
                        if ($obj->once) {
                            unset($self->listeners[$priority][$eventName][$idx]);
                        }

                        $output = call_user_func_array($obj->listener, array_merge(array($eventData), $args));
                        $previousEvent = $eventData;

                        if ($isBufferEvent && $output) {
                            $buffer = $output;
                            array_shift($args);
                            array_unshift($args, $buffer);
                        }

                        if ($output && (is_a($output, 'Object') || is_subclass_of($output, 'Object')) && !$output->toBool()) {
                            return $output;
                        }
                    }
                }
            }
        }

        if ($isBufferEvent) {
            return $buffer;
        }

        return new Object();
    }

    function _isStandalone($eventName)
    {
        $tmp = explode('/', $_SERVER['SCRIPT_NAME']);
        if (empty($tmp[0])) unset($tmp[0]);
        $tmp = array_reverse($tmp);
        $tmp[0] = preg_replace('/\.[^.]+$/', '', $tmp[0]);
        $scriptName = implode('.', $tmp);

        return $eventName == $scriptName;
    }

    function _moduleExtendRun()
    {
        $oInstallModel = & getModel('install');
        $moduleList = $oInstallModel->getModuleList();
        foreach ($moduleList as $moduleName) {
            $oModuleExtend = & getModule($moduleName, 'extend');

            if ($oModuleExtend && method_exists($oModuleExtend, 'run')) {
                $oModuleExtend->run();
            }
        }
    }

    function _emitPage($page)
    {
        $tmp = explode('/', str_replace($_SERVER['DOCUMENT_ROOT'], '', $page));
        if (empty($tmp[0])) unset($tmp[0]);
        $tmp = array_reverse($tmp);
        $tmp[0] = preg_replace('/\.[^.]+$/', '', $tmp[0]);
        $eventName = implode('.', $tmp);
        $output = $this->emit($eventName, $_REQUEST);

        if ($output && (is_a($output, 'Object') || is_subclass_of($output, 'Object')) && !$output->toBool()) {
            alert($output->getMessage());
        }
    }

    function _init()
    {
        if (!$this->isInitialize) {
            $this->_moduleExtendRun();

            $this->emit(X_EVENT_INIT);
            $this->isInitialize = true;

            if (Context::get('module') && Context::get('act')) {
                $this->emit(sprintf('%s.%s.x', Context::get('act'), Context::get('module')));
            }
        }

        $this->_loadIncluded();
    }

    function _loadIncluded()
    {
        static $included = array();

        $inc = get_included_files();
        foreach (array_diff($inc, $included) as $files) {
            $this->_emitPage($files);
        }

        $included = $inc;
    }

    function _start()
    {
        if (!$this->isInitialize) return;
        $this->_loadIncluded();
        if ($this->isStarted) return;

        $this->emit(X_EVENT_START);
        $this->isStarted = true;

        ob_start();
        $this->startOutputLevel = ob_get_level();
    }

    function _close()
    {
        if (!$this->isStarted) return;
        $this->_loadIncluded();

        if (ob_get_level() > $this->startOutputLevel) return;

        $buffer = ob_get_clean();
        $buffer = $this->emit(X_EVENT_CLOSE, $buffer);

        $buffer = $this->_transModuleProcess($buffer);
        $buffer = $this->emit(X_EVENT_MODULE_LOADED, $buffer);

        require_once sprintf('%s/classes/display/HTMLDisplayHandler.php', _X_PATH_);
        HTMLDisplayHandler::_loadJSCSS();

        $current_url = getFullUrl();
        $request_uri = Context::getRequestUri();
        $xScriptConstant = <<<CONSTANT
<script type="text/javascript">
var current_url = "{$current_url}";
var request_uri = "{$request_uri}";
</script>
CONSTANT;
        Context::addHtmlHeader($xScriptConstant);

        $buffer = $this->_htmlProcess($buffer);
        $buffer = $this->emit(X_EVENT_END, $buffer);

        print $buffer;
    }

    function _transModuleProcess($buffer)
    {
        return preg_replace_callback('!<img[^\>]*module=[^\>]*?\>!is', array($this, '_transModule'), $buffer);
    }

    function _htmlProcess($buffer)
    {
        $buffer = $this->_addHtmlHeader($buffer, Context::getHtmlHeader());
        $buffer = $this->_addBodyClass($buffer, Context::getBodyClass());
        $buffer = $this->_addBodyHeader($buffer, Context::getBodyHeader());
        $buffer = $this->_addHtmlFooter($buffer, Context::getHtmlFooter());

        $headerBuffer = '';
        foreach (Context::getJsFile('head') as $js) {
            $temp = '';
            if ($js['targetie']) {
                $temp = sprintf('<!--[if %s]>', $js['targetie']);
            }
            $temp .= sprintf('<script type="text/javascript" src="%s" ></script>', $js['file']);
            if ($js['targetie']) {
                $temp .= '<![endif]-->';
            }
            $headerBuffer .= "\n" . $temp;
        }

        foreach (Context::getCSSFile() as $css) {
            $temp = '';
            if ($css['targetie']) {
                $temp = sprintf('<!--[if %s]>', $css['targetie']);
            }
            $temp .= sprintf('<link rel="stylesheet" href="%s"', $css['file']);

            if ($css['media']) {
                $temp .= sprintf(' media="%s"', $css['media']);
            }
            $temp .= ' />';
            if ($css['targetie']) {
                $temp .= '<![endif]-->';
            }

            $headerBuffer .= "\n" . $temp;
        }

        $buffer = $this->_addHtmlHeader($buffer, $headerBuffer);

        $footerBuffer = '';
        foreach (Context::getJsFile('body') as $js) {
            $temp = '';
            if ($css['targetie']) {
                $temp = sprintf('<!--[if %s]>', $js['targetie']);
            }
            $temp .= sprintf('<script type="text/javascript" src="%s" ></script>', $js['file']);
            if ($js['targetie']) {
                $temp .= sprintf('<!--[if %s]>', $js['targetie']);
            }
            $footerBuffer .= $temp . "\n";
        }

        $buffer = $this->_addHtmlFooter($buffer, $footerBuffer);

        return $buffer;
    }

    /**
     * @brief Widget code with the actual code changes
     */
    function _transModule($matches)
    {
        $buff = trim($matches[0]);
        $oXmlParser = new XmlParser();
        $xml_doc = $oXmlParser->parse($buff);
        if ($xml_doc->img) $vars = $xml_doc->img->attrs;
        else $vars = $xml_doc->attrs;

        $module = $vars->module;
        $act = $vars->act;
        if (!$module || !$act) return $matches[0];
        unset($vars->module);
        unset($vars->act);

        return $this->_execute($module, $act, $vars);
    }

    /**
     * @brief Widget name and argument and produce a result and Return the results
     * Tags used in templateHandler $this-&gt; execute() will be replaced by the code running
     *
     * $Javascript_mode is true when editing your page by the code for handling Includes photos
     */
    function _execute($module, $act, $args)
    {
        $object_vars = get_object_vars($args);
        if (count($object_vars)) {
            foreach ($object_vars as $key => $val) {
                Context::set($key, $val);
            }
        }

        ob_start();
        $oModuleHandler = new ModuleHandler($module, $act);
        try {
            if ($oModuleHandler->init()) {
                $oModule = & $oModuleHandler->procModule();
                $oModuleHandler->displayContent($oModule);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        $buffer = ob_get_contents();
        ob_end_clean();

        return $buffer;
    }

    function _addHtmlHeader($buffer, $header)
    {
        if (empty($header)) return $buffer;

        $header = str_replace('"', '\"', $header);
        $code = <<<CODE
            return sprintf('%s%s</head>', substr(\$match[0], 0, strlen(\$match[0]) - strlen('</head>')), "{$header}");
CODE;

        $func = create_function('$match', $code);
        return preg_replace_callback('!<head[^>]*>(.*?)</head>!is', $func, $buffer, 1);
    }


    function _addBodyClass($buffer, $class)
    {
        if (empty($class)) return $buffer;

        $code = <<<CODE
            if(preg_match('!class\s*=\s*!is', \$match[0])){
                return preg_replace('!class\s*=\s*(?:(?:[\'\"])([^\'\"]+)(?:[\'\" ])|([^ ]+))!is', sprintf('class="$1$2 %s"', '{$class}'), \$match[0]);
            }else{
                return substr(\$match[0], 0, -1).sprintf(' class="%s">', '{$class}');
            }
CODE;
        $func = create_function('$match', $code);
        return preg_replace_callback('!<body([^>]*)>!is', $func, $buffer, 1);
    }


    function _addBodyHeader($buffer, $header)
    {
        if (empty($header)) return $buffer;

        $header = str_replace('"', '\"', $header);
        $code = <<<CODE
            return sprintf("%s\\n%s", \$match[0], "{$header}");
CODE;
        $func = create_function('$match', $code);
        return preg_replace_callback('!<body([^>]*)>!is', $func, $buffer, 1);
    }


    function _addHtmlFooter($buffer, $footer)
    {
        if (empty($footer)) return $buffer;

        $footer = str_replace('"', '\"', $footer);
        $code = <<<CODE
            return sprintf("%s\\n%s", "{$footer}", \$match[0]);
CODE;
        $func = create_function('$match', $code);
        return preg_replace_callback('!</body>!is', $func, $buffer, 1);
    }
}