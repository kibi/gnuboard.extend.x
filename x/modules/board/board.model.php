<?php

class boardModel extends board
{
    function getBoardInfoByBoardName($boardName)
    {
        global $g5;
        $output = executeQuery("select * from `{$g5['board_table']}` where `bo_table` = '{$boardName}'");
        return $output->data;
    }

    function getDocumentInfoByBoardNameAndWriteId($boardName, $writeId)
    {
        global $g5;

        $boardTable = $g5['write_prefix'] . $boardName;
        $output = executeQuery("select * from `{$boardTable}` where `wr_id` = '{$writeId}'");
        return $output->data;
    }
}