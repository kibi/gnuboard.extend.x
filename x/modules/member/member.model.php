<?php

class memberModel extends member
{
    function init()
    {

    }

    /**
     * @brief Check if logged-in
     */
    function isLogged()
    {
        return !!$_SESSION['ss_mb_id'];
    }

    /**
     * @brief Return session information of the logged-in user
     */
    function getLoggedInfo()
    {
        if ($this->isLogged()) {
            if (Context::get('logged_info')) {
                return Context::get('logged_info');
            } else {
                $logged_info = $this->getMemberInfoByMemberId($_SESSION['ss_mb_id']);

                if ($logged_info) {
                    Context::set('logged_info', $logged_info);
                    return $logged_info;
                }
            }
        }
        return null;
    }

    function getMemberInfoByMemberNo($memberNo)
    {
        $g5 = & $GLOBALS['g5'];
        if (!$memberNo) return;

        if (!$GLOBALS['X_MEMBER_INFO']['NO'][$memberNo]) {
            $output = executeQuery("select * from {$g5['member_table']} where mb_no = '{$memberNo}'");
            if (!$output->data) return;

            $this->arrangeMemberInfo($output->data);
        }


        return $GLOBALS['X_MEMBER_INFO']['NO'][$memberNo];
    }

    function getMemberInfoByMemberId($memberId)
    {
        $g5 = & $GLOBALS['g5'];
        if (!$memberId) return;

        if (!$GLOBALS['X_MEMBER_INFO']['ID'][$memberId]) {
            $output = executeQuery("select * from {$g5['member_table']} where mb_id = '{$memberId}'");
            if (!$output->data) return;

            $this->arrangeMemberInfo($output->data);
        }

        return $GLOBALS['X_MEMBER_INFO']['ID'][$memberId];

    }

    function findMemberByContact($contact, $options = null, &$navigation = null)
    {
        if (!$contact) return null;
        $contact = preg_replace('/[^0-9]/', '', $contact);

        $memberList = $this->getMemberList("replace(mb_hp, '-', '') = '{$contact}' or replace(mb_hp, '-', '')='{$contact}'", $options, $navigation);
        if (!$memberList) return null;

        $output = array();
        foreach ($memberList as $memberInfo) {
            $output[] = $this->arrangeMemberInfo($memberInfo);
        }

        return $output;
    }

    function getMemberList($condition = null, $options = null, &$navigation = null)
    {
        $g5 = & $GLOBALS['g5'];

        $query = new Query($options);

        if ($condition) $condition = ' where ' . $condition;
        $output = $query->executeQueryArray("select * from `{$g5['member_table']}` {$condition}");

        $navigation = $output->navigation;
        return $output->data;
    }

    function arrangeMemberInfo($info)
    {
        if (!$GLOBALS['X_MEMBER_INFO']['NO'][$info->mb_no]) {
            $GLOBALS['X_MEMBER_INFO']['NO'][$info->mb_no] = $info;
        }

        if (!$GLOBALS['X_MEMBER_INFO']['ID'][$info->mb_id]) {
            $GLOBALS['X_MEMBER_INFO']['ID'][$info->mb_id] = $info;
        }

        return $GLOBALS['X_MEMBER_INFO']['NO'][$info->mb_no];
    }
}