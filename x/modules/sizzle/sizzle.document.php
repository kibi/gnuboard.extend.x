<?php
require_once 'lib/phpQuery/phpQuery-onefile.php';

class sizzleDocument
{
    var $documentWrapper;

    public function sizzleDocument($markup, $charset = null)
    {
        //자바스크립트 내 태그 처리
        $func = create_function('$matches', 'return $matches[1].preg_replace("@</@i", "<\\/", $matches[2]).$matches[3];');
        $markup = preg_replace_callback('@(<script[^>]*>)([\s\S]*?)(</script>)@i', $func, $markup);

        //제어 아스키코드 처리
//        $func = create_function('$matches', 'return sprintf("U+%04d", ord($matches[1]));');
//        $markup = preg_replace_callback('/([\x1E])/', $func, $markup);

        phpQuery::unloadDocuments();
        $this->documentWrapper = phpQuery::newDocumentHTML($markup, $charset);
    }

    function newElement($element)
    {
        return pq($element, $this->documentWrapper);
    }

    function find()
    {
        $args = func_get_args();
        return call_user_func_array(array($this->documentWrapper, 'find'), $args);
    }

    function __toString()
    {
        $args = func_get_args();
        $output = call_user_func_array(array($this->documentWrapper, '__toString'), $args);

        //제어 아스키코드 복원
//        $func = create_function('$matches', 'return chr($matches[1]);');
//        $output = preg_replace_callback('/U\+([0-9]{4})/', $func, $output);

        return $output;
    }

    function __destruct()
    {
        if (method_exists($this->documentWrapper, '__destruct')) {
            call_user_func_array(array($this->documentWrapper, '__destruct'), func_get_args());
        }
        $this->documentWrapper = null;
    }

}