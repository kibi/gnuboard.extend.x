<?php
require_once 'sizzle.document.php';
class sizzleModel extends sizzle
{
    function newDocument($markup, $charset = null)
    {
        return new sizzleDocument($markup, $charset);
    }
}