<?php

class installModel extends install
{
    function isModuleInstalled($moduleName)
    {
        $moduleInfo = $this->getCacheHandler()->get(sprintf('install:%s', $moduleName));
        if (!$moduleInfo) return false;

        $moduleSchemas = $this->getModuleSchemas($moduleName);
        if(count($moduleSchemas) != count($moduleInfo->schemas)) return false;

        foreach($moduleSchemas as $schema){
            if(!in_array($schema, $moduleInfo->schemas)) return false;
        }

        return true;
    }

    function getModuleList()
    {
        if (!$GLOBALS['X_MODULE_LIST']) {
            $GLOBALS['X_MODULE_LIST'] = FileHandler::readDir('./modules');
        }

        return $GLOBALS['X_MODULE_LIST'];
    }

    function getModuleSchemas($moduleName)
    {
        $oModule = & getClass($moduleName);
        $schemaDir = sprintf('%s/schemas/', $oModule->module_path);
        $schemaFiles = FileHandler::readDir($schemaDir, '/(\.sql)$/i', false, true);

        return $schemaFiles;
    }
}