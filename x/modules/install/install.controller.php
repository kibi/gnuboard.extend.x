<?php

class installController extends install
{
    function installModule($moduleName)
    {
        $oInstallModel = & getModel('install');
        if ($oInstallModel->isModuleInstalled($moduleName)) return new Object();

        $oModule = & getClass($moduleName);
        $schemaFiles = $oInstallModel->getModuleSchemas($moduleName);

        $moduleInfo = new stdClass();
        $moduleInfo->name = $moduleName;
        $moduleInfo->module_path = $oModule->module_path;
        $moduleInfo->schemas = array();

        $oDB = & DB::getInstance();
        $oDB->begin();

        foreach ($schemaFiles as $schemaFile) {
            $tableName = preg_replace('@^.*?([^\/]+)\.sql$@i', '$1', $schemaFile);
            if ((preg_match('/^sp_/i', $tableName) && DB::isProcedureExists($tableName)) || DB::isTableExists($tableName)) {
                $moduleInfo->schemas[] = $schemaFile;
            } else {
                $query = FileHandler::readFile('./' . $schemaFile);
                $output = $oDB->executeQuery($query);
                if (!$output->toBool()) {
                    $oDB->rollback();
                    return $output;
                } else {
                    $moduleInfo->schemas[] = $schemaFile;
                }
            }
        }

        $oDB->commit();
        $this->saveModuleInfo($moduleName, $moduleInfo);

        if (method_exists($oModule, 'moduleInstall')) {
            $output = $oModule->moduleInstall();
            if (is_a($output, 'Object')) {
                return $output;
            }
        }

        return new Object();
    }

    function updateModule($moduleName)
    {
        $oModule = & getClass($moduleName);

        //설치여부 확인
        $output = $this->installModule($moduleName);
        if (!$output->toBool()) return $output;


        //모듈 업데이트
        if (@method_exists($oModule, 'checkUpdate') && $oModule->checkUpdate()) {
            if (@method_exists($oModule, 'moduleUpdate')) {
                $output = $oModule->moduleUpdate();
                if (is_a($output, 'Object')) {
                    return $output;
                }
            }
        }

        return new Object();
    }

    function removeCache($moduleName)
    {
        $this->getCacheHandler()->delete(sprintf('install:%s', $moduleName));
        return new Object();
    }

    function saveModuleInfo($moduleName, $moduleInfo)
    {
        $this->getCacheHandler()->put(sprintf('install:%s', $moduleName), $moduleInfo);
        return new Object();
    }
}