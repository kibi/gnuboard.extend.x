<?php

class install extends ModuleObject
{
    static $oCacheHandler;

    function install(){
        self::$oCacheHandler = & CacheHandler::getInstance('object', null, true);
    }

    function getCacheHandler(){
        return self::$oCacheHandler;
    }
}