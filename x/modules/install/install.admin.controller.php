<?php

class installAdminController extends ModuleObject
{
    function procInstallAdminInstall()
    {
        $oInstallModel = & getModel('install');
        $oInstallController = & getController('install');

        $moduleList = $oInstallModel->getModuleList();
        foreach ($moduleList as $moduleName) {
            $output = $oInstallController->installModule($moduleName);
            if (!$output->toBool()) return $output;
        }

        return new Object();
    }

    function procInstallAdminUpdate()
    {
        $oInstallModel = & getModel('install');
        $oInstallController = & getController('install');

        $moduleList = $oInstallModel->getModuleList();
        foreach ($moduleList as $moduleName) {
            $output = $oInstallController->updateModule($moduleName);
            if (!$output->toBool()) return $output;
        }

        return new Object();
    }

    function procInstallAdminRemoveCache()
    {
        $oInstallModel = & getModel('install');
        $oInstallController = & getController('install');

        $moduleList = $oInstallModel->getModuleList();
        foreach ($moduleList as $moduleName) {
            $output = $oInstallController->removeCache($moduleName);
            if (!$output->toBool()) return $output;
        }

        return new Object();

    }
}