<?php

class shopModel extends shop
{
    /**
     * 주문정보 반환
     *
     * @param string $orderId 주문코드
     * @return object 주문정보
     */
    function getShopOrderInfoByOrderId($orderId)
    {
        global $g5;
        $output = executeQuery("select * from `{$g5['g5_shop_order_table']}` where `od_id` = '{$orderId}'");
        return $output->data;
    }

    /**
     * 주문의 상품목록을 반환
     *
     * @param string $orderId 주문코드
     * @return array 상품목록
     */
    function getShopOrderItemsByOrderId($orderId)
    {
        global $g5;

        $output = executeQueryArray("select * from `{$g5['g5_shop_cart_table']}` where `od_id` = '{$orderId}'");

        return $output->data;
    }


    /**
     * 카트 정보 반환
     * @param string $cartId 장바구니코드
     * @return object
     */
    function getShopOrderItemByCartId($cartId)
    {
        global $g5;

        $output = executeQuery("select * from `{$g5['g5_shop_cart_table']}` where `ct_id` = '{$cartId}'");

        return $output->data;
    }

    function getShopOrderItemList($condition = null, $options = null, &$navigation = null)
    {
        global $g5;
        $query = new Query($options);

        if ($condition) $condition = ' where ' . $condition;
        $output = $query->executeQueryArray("select * from `{$g5['g5_shop_cart_table']}`{$condition}");

        $navigation = $output->navigation;
        return $output->data;
    }
}