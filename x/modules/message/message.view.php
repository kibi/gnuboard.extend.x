<?php
class messageView extends message
{
    /**
     * @brief Display messages
     */
    function dispMessage()
    {
        // Get configurations (using module model object)
        $config = new stdClass();

        $config->skin = 'default';
        $template_path = sprintf('%sskins/%s', $this->module_path, $config->skin);

        // Template path
        $this->setTemplatePath($template_path);
        Context::set('system_message', nl2br($this->getMessage()));

        $this->setTemplateFile('system_message');
    }
}
/* End of file message.view.php */
/* Location: ./modules/message/message.view.php */
