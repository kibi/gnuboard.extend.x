'use strict';
require.config({
  baseUrl: '/x/common/js',
  map: {
    '*': {
      'jquery': 'jquery-private'
    },
    'jquery-private': {
      'jquery': 'jquery'
    }
  },
  paths: {
    // plugins
    'async': 'libs/require/plugins/async',
    'depend': 'libs/require/plugins/depend',
    'font': 'libs/require/plugins/font',
    'goog': 'libs/require/plugins/goog',
    'image': 'libs/require/plugins/image',
    'json': 'libs/require/plugins/json',
    'noext': 'libs/require/plugins/noext',
    'mdown': 'libs/require/plugins/mdown',
    'propertyParser': 'libs/require/plugins/propertyParser',
    'text': 'libs/require/plugins/text',
    'order': 'libs/require/plugins/order',
    'i18n': 'libs/require/plugins/i18n',
    'css': 'libs/require/plugins/css',

    // libs
    'jquery-private': 'libs/jquery/' + ('querySelector' in document && 'localStorage' in window && 'addEventListener' in window ? '2.1.0' : '1.11.0') + '/jquery.min',
    'jquery-ui': 'libs/jquery-ui/1.10.4/jquery-ui.min',

    'underscore': 'libs/underscore/1.5.2/underscore.min'
  },
  shim: {
    'jquery-private': {
      init: function () {
        return jQuery.noConflict(true);
      },
      exports: 'jQuery'
    },
    'jquery-ui': {
      deps: ['jquery', 'css!../css/jquery-ui/jquery-ui-1.10.4.min.css'],
      exports: 'jQuery'
    },
    underscore: {
      exports: '_'
    }
  },
  priority: [
    'jquery', 'underscore', 'angular'
  ],
  urlArgs: 't=' + (new Date()).getTime(),
  waitSeconds: 60
});

(function() {
  var method;
  var noop = function () {};
  var methods = [
    'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
    'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
    'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
    'timeStamp', 'trace', 'warn'
  ];
  var console = (window.console = window.console || {});

  for(var i=0;i<methods.length;i++){
    method = methods[i];

    // Only stub undefined methods.
    if (!console[method]) {
      console[method] = noop;
    }
  }
}());