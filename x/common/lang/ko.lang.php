<?php
$GLOBALS['lang']->success = '완료되었습니다.';
$GLOBALS['lang']->msg_db_not_setted = 'DB 설정이 되어 있지 않습니다.';
$GLOBALS['lang']->msg_module_is_not_exists = '요청한 페이지를 찾을 수 없습니다. 사이트 관리자에게 문의해 주세요.';
$GLOBALS['lang']->msg_not_found_act = '요청한 페이지를 찾을 수 없습니다. 사이트 관리자에게 문의해 주세요.';
$GLOBALS['lang']->msg_invalid_request = '잘못된 요청입니다.';
$GLOBALS['lang']->msg_require_logged = '로그인이 필요합니다. 로그인 후 다시 시도해주세요.';
$GLOBALS['lang']->msg_exceeds_limit_size = '허용된 용량을 초과하여 첨부가 되지 않았습니다.';
$GLOBALS['lang']->msg_not_permitted_create = '파일 또는 디렉터리를 생성할 수 없습니다.';
$GLOBALS['lang']->msg_not_permitted_delete = '삭제권한이 없습니다.';
$GLOBALS['lang']->msg_file_upload_error = '파일 업로드 중 에러가 발생하였습니다.';
$GLOBALS['lang']->msg_require_member_id = '아이디를 입력해주세요.';
$GLOBALS['lang']->msg_exists_member = '이미 회원이 존재합니다.';
$GLOBALS['lang']->msg_not_exists_member = '회원이 존재하지 않습니다.';
?>