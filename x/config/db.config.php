<?php
if (!defined('__X__')) exit;

$db_info = new stdClass();
$db_info->db_type = 'mysqli';
$db_info->db_hostname = G5_MYSQL_HOST;
$db_info->db_port = 3306;
$db_info->db_userid = G5_MYSQL_USER;
$db_info->db_password = G5_MYSQL_PASSWORD;
$db_info->db_database = G5_MYSQL_DB;
$db_info->use_object_cache = 'file';
$db_info->use_template_cache = 'file';