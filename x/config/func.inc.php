<?php
if (!defined('__X__')) exit();

// define clone for php5
if (version_compare(phpversion(), '5.0') < 0) {
    eval('
      function clone($object) {
      return $object;
      }
    ');
}

// define an empty function to avoid errors when iconv function doesn't exist
if (!function_exists('iconv')) {
    eval('
    function iconv($in_charset, $out_charset, $str) {
        return $str;
    }
  ');
}

if (!function_exists('htmlspecialchars_decode')) {
    /**
     * Decode html special characters
     *
     * @param string $text
     * @return string
     */
    function htmlspecialchars_decode($text)
    {
        return strtr($text, array_flip(get_html_translation_table(HTML_SPECIALCHARS)));
    }
}

/**
 * Define a function to use {@see ModuleHandler::getModuleObject()} ($module_name, $type)
 *
 * @param string $module_name The module name to get a instance
 * @param string $type disp, proc, controller, class
 * @param string $kind admin, null
 * @return mixed Module instance
 */
function getModule($module_name, $type = 'view', $kind = '')
{
    return ModuleHandler::getModuleInstance($module_name, $type, $kind);
}

/**
 * Create a controller instance of the module
 *
 * @param string $module_name The module name to get a controller instance
 * @return mixed Module controller instance
 */
function getController($module_name)
{
    return getModule($module_name, 'controller');
}

/**
 * Create a admin controller instance of the module
 *
 * @param string $module_name The module name to get a admin controller instance
 * @return mixed Module admin controller instance
 */
function getAdminController($module_name)
{
    return getModule($module_name, 'controller', 'admin');
}

/**
 * Create a view instance of the module
 *
 * @param string $module_name The module name to get a view instance
 * @return mixed Module view instance
 */
function getView($module_name)
{
    return getModule($module_name, 'view');
}

/**
 * Create a admin view instance of the module
 *
 * @param string $module_name The module name to get a admin view instance
 * @return mixed Module admin view instance
 */
function getAdminView($module_name)
{
    return getModule($module_name, 'view', 'admin');
}

/**
 * Create a model instance of the module
 *
 * @param string $module_name The module name to get a model instance
 * @return mixed Module model instance
 */
function getModel($module_name)
{
    return getModule($module_name, 'model');
}

/**
 * Create an admin model instance of the module
 *
 * @param string $module_name The module name to get a admin model instance
 * @return mixed Module admin model instance
 */
function getAdminModel($module_name)
{
    return getModule($module_name, 'model', 'admin');
}

/**
 * Create an api instance of the module
 *
 * @param string $module_name The module name to get a api instance
 * @return mixed Module api class instance
 */
function getAPI($module_name)
{
    return getModule($module_name, 'api');
}

/**
 * Create a class instance of the module
 *
 * @param string $module_name The module name to get a class instance
 * @return mixed Module class instance
 */
function getClass($module_name)
{
    return getModule($module_name, 'class');
}

/**
 * The alias of DB::executeQuery()
 *
 * @see DB::executeQuery()
 * @param string $query
 * @return object Query result data
 */
function executeQuery($query)
{
    $oDB = DB::getInstance();
    return $oDB->executeQuery($query);
}

function executeQueryArray($query)
{
    $output = executeQuery($query);
    if (!is_array($output->data) && count($output->data) > 0) {
        $output->data = array($output->data);
    }else if(!isset($output->data)){
        $output->data = array();
    }
    return $output;
}

function executeProcedure($procedure, $params)
{
    if (is_object($params)) {
        $params = (array)$params;
    }
    if (!is_array($params)) return new Object(-1, 'msg_invalid_params');


    $args = "'" . implode("','", array_values($params)) . "'";
    $output = executeQuery(sprintf('CALL %s(%s);', $procedure, $args));

    return $output;
}

function executeProcedureArray($procedure, $param = null)
{
    $output = executeProcedure($procedure, $param);

    if (!is_array($output->data) && count($output->data) > 0) {
        $output->data = array($output->data);
    }

    return $output;
}

/**
 * Get a encoded url. Define a function to use Context::getUrl()
 *
 * getUrl() returns the URL transformed from given arguments of RequestURI
 * <ol>
 *  <li>argument format follows as (key, value).
 * ex) getUrl('key1', 'val1', 'key2',''): transform key1 and key2 to val1 and '' respectively</li>
 * <li>returns URL without the argument if no argument is given.</li>
 * <li>URL made of args_list added to RequestUri if the first argument value is ''.</li>
 * </ol>
 *
 * @return string
 */
function getUrl()
{
    $num_args = func_num_args();
    $args_list = func_get_args();

    if ($num_args)
        $url = Context::getUrl($num_args, $args_list);
    else
        $url = Context::getRequestUri();

    return preg_replace('@\berror_return_url=[^&]*|\w+=(?:&|$)@', '', $url);
}

/**
 * Get a not encoded(html entity) url
 *
 * @see getUrl()
 * @return string
 */
function getNotEncodedUrl()
{
    $num_args = func_num_args();
    $args_list = func_get_args();

    if ($num_args) {
        $url = Context::getUrl($num_args, $args_list, NULL, FALSE);
    } else {
        $url = Context::getRequestUri();
    }

    return preg_replace('@\berror_return_url=[^&]*|\w+=(?:&|$)@', '', $url);
}

/**
 * Get a encoded url. If url is encoded, not encode. Otherwise html encode the url.
 *
 * @see getUrl()
 * @return string
 */
function getAutoEncodedUrl()
{
    $num_args = func_num_args();
    $args_list = func_get_args();

    if($num_args)
    {
        $url = Context::getUrl($num_args, $args_list, NULL, TRUE, TRUE);
    }
    else
    {
        $url = Context::getRequestUri();
    }

    return preg_replace('@\berror_return_url=[^&]*|\w+=(?:&|$)@', '', $url);
}

/**
 * Return the value adding request uri to getUrl() to get the full url
 *
 * @return string
 */
function getFullUrl()
{
    $num_args = func_num_args();
    $args_list = func_get_args();
    $request_uri = Context::getRequestUri();
    if(!$num_args)
    {
        return $request_uri;
    }

    $url = Context::getUrl($num_args, $args_list);
    if(strncasecmp('http', $url, 4) !== 0)
    {
        preg_match('/^(http|https):\/\/([^\/]+)\//', $request_uri, $match);
        return substr($match[0], 0, -1) . $url;
    }
    return $url;
}

/**
 * Return the value adding request uri to getUrl() to get the not encoded full url
 *
 * @return string
 */
function getNotEncodedFullUrl()
{
    $num_args = func_num_args();
    $args_list = func_get_args();
    $request_uri = Context::getRequestUri();
    if(!$num_args)
    {
        return $request_uri;
    }

    $url = Context::getUrl($num_args, $args_list, NULL, FALSE);
    if(strncasecmp('http', $url, 4) !== 0)
    {
        preg_match('/^(http|https):\/\/([^\/]+)\//', $request_uri, $match);
        $url = Context::getUrl($num_args, $args_list, NULL, FALSE);
        return substr($match[0], 0, -1) . $url;
    }
    return $url;
}

/**
 * getSiteUrl() returns the URL by transforming the given argument value of domain
 * The first argument should consist of domain("http://" not included) and path
 *
 * @return string
 */
function getSiteUrl()
{
    $num_args = func_num_args();
    $args_list = func_get_args();

    if(!$num_args)
    {
        return Context::getRequestUri();
    }

    $domain = array_shift($args_list);
    $num_args = count($args_list);

    return Context::getUrl($num_args, $args_list, $domain);
}

/**
 * getSiteUrl() returns the not encoded URL by transforming the given argument value of domain
 * The first argument should consist of domain("http://" not included) and path
 *
 * @return string
 */
function getNotEncodedSiteUrl()
{
    $num_args = func_num_args();
    $args_list = func_get_args();

    if(!$num_args)
    {
        return Context::getRequestUri();
    }

    $domain = array_shift($args_list);
    $num_args = count($args_list);

    return Context::getUrl($num_args, $args_list, $domain, FALSE);
}

/**
 * Return the value adding request uri to the getSiteUrl() To get the full url
 *
 * @return string
 */
function getFullSiteUrl()
{
    $num_args = func_num_args();
    $args_list = func_get_args();

    $request_uri = Context::getRequestUri();
    if(!$num_args)
    {
        return $request_uri;
    }

    $domain = array_shift($args_list);
    $num_args = count($args_list);

    $url = Context::getUrl($num_args, $args_list, $domain);
    if(strncasecmp('http', $url, 4) !== 0)
    {
        preg_match('/^(http|https):\/\/([^\/]+)\//', $request_uri, $match);
        return substr($match[0], 0, -1) . $url;
    }
    return $url;
}

/**
 * Put a given tail after trimming string to the specified size
 *
 * @param string $string The original string to trim
 * @param int $cut_size The size to be
 * @param string $tail Tail to put in the end of the string after trimming
 * @return string
 **/
function cutString($string, $cut_size = 0, $tail = '...')
{
    if ($cut_size < 1 || !$string) return $string;

    if ($GLOBALS['use_mb_strimwidth'] || function_exists('mb_strimwidth')) {
        $GLOBALS['use_mb_strimwidth'] = TRUE;
        return mb_strimwidth($string, 0, $cut_size + 4, $tail, 'utf-8');
    }

    $chars = array(12, 4, 3, 5, 7, 7, 11, 8, 4, 5, 5, 6, 6, 4, 6, 4, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 4, 4, 8, 6, 8, 6, 10, 8, 8, 9, 8, 8, 7, 9, 8, 3, 6, 7, 7, 11, 8, 9, 8, 9, 8, 8, 7, 8, 8, 10, 8, 8, 8, 6, 11, 6, 6, 6, 4, 7, 7, 7, 7, 7, 3, 7, 7, 3, 3, 6, 3, 9, 7, 7, 7, 7, 4, 7, 3, 7, 6, 10, 6, 6, 7, 6, 6, 6, 9);
    $max_width = $cut_size * $chars[0] / 2;
    $char_width = 0;

    $string_length = strlen($string);
    $char_count = 0;

    $idx = 0;
    while ($idx < $string_length && $char_count < $cut_size && $char_width <= $max_width) {
        $c = ord(substr($string, $idx, 1));
        $char_count++;
        if ($c < 128) {
            $char_width += (int)$chars[$c - 32];
            $idx++;
        } else if (191 < $c && $c < 224) {
            $char_width += $chars[4];
            $idx += 2;
        } else {
            $char_width += $chars[0];
            $idx += 3;
        }
    }

    $output = substr($string, 0, $idx);
    if (strlen($output) < $string_length) {
        $output .= $tail;
    }

    return $output;
}

/**
 * YYYYMMDDHHIISS format changed to unix time value
 *
 * @param string $str Time value in format of YYYYMMDDHHIISS
 * @return int
 **/
function ztime($str)
{
    if (!$str) return;
    $hour = (int)substr($str, 8, 2);
    $min = (int)substr($str, 10, 2);
    $sec = (int)substr($str, 12, 2);
    $year = (int)substr($str, 0, 4);
    $month = (int)substr($str, 4, 2);
    $day = (int)substr($str, 6, 2);
    if (strlen($str) <= 8) {
        $gap = 0;
    } else {
        $gap = zgap();
    }

    return mktime($hour, $min, $sec, $month ? $month : 1, $day ? $day : 1, $year) + $gap;
}

/**
 * If the recent post within a day, output format of YmdHis is "min/hours ago from now". If not within a day, it return format string.
 *
 * @param string $date Time value in format of YYYYMMDDHHIISS
 * @param string $format If gap is within a day, returns this format.
 * @return string
 **/
function getTimeGap($date, $format = 'Y.m.d')
{
    $gap = time() + zgap() - ztime($date);

    $lang_time_gap = Context::getLang('time_gap');
    if ($gap < 60) $buff = sprintf($lang_time_gap['min'], (int)($gap / 60) + 1);
    elseif ($gap < 60 * 60) $buff = sprintf($lang_time_gap['mins'], (int)($gap / 60) + 1);
    elseif ($gap < 60 * 60 * 2) $buff = sprintf($lang_time_gap['hour'], (int)($gap / 60 / 60) + 1);
    elseif ($gap < 60 * 60 * 24) $buff = sprintf($lang_time_gap['hours'], (int)($gap / 60 / 60) + 1);
    else $buff = zdate($date, $format);
    return $buff;
}

/**
 * Name of the month return
 *
 * @param int $month Month
 * @param boot $short If set, returns short string
 * @return string
 **/
function getMonthName($month, $short = true)
{
    $short_month = array('', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
    $long_month = array('', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
    return !$short ? $long_month[$month] : $short_month[$month];
}

/**
 * Change the time format YYYYMMDDHHIISS to the user defined format
 *
 * @param string|int $str YYYYMMDDHHIISS format time values
 * @param string $format Time format of php date() function
 * @param bool $conversion Means whether to convert automatically according to the language
 * @return string
 **/
function zdate($str, $format = 'Y-m-d H:i:s', $conversion = false)
{
    // return null if no target time is specified
    if (!$str) return;
    // convert the date format according to the language
    if ($conversion == true) {
    }

    // If year value is less than 1970, handle it separately.
    if ((int)substr($str, 0, 4) < 1970) {
        $hour = (int)substr($str, 8, 2);
        $min = (int)substr($str, 10, 2);
        $sec = (int)substr($str, 12, 2);
        $year = (int)substr($str, 0, 4);
        $month = (int)substr($str, 4, 2);
        $day = (int)substr($str, 6, 2);

        // leading zero?
        $lz = create_function('$n', 'return ($n>9?"":"0").$n;');

        $trans = array(
            'Y' => $year,
            'y' => $lz($year % 100),
            'm' => $lz($month),
            'n' => $month,
            'd' => $lz($day),
            'j' => $day,
            'G' => $hour,
            'H' => $lz($hour),
            'g' => $hour % 12,
            'h' => $lz($hour % 12),
            'i' => $lz($min),
            's' => $lz($sec),
            'M' => getMonthName($month),
            'F' => getMonthName($month, false)
        );

        $string = strtr($format, $trans);
    } else {
        // if year value is greater than 1970, get unixtime by using ztime() for date() function's argument.
        $string = date($format, ztime($str));
    }

    return $string;
}

/**
 * Get a time gap between server's timezone and XE's timezone
 *
 * @return int
 */
function zgap()
{
    $time_zone = $GLOBALS['_time_zone'];
    if($time_zone < 0)
    {
        $to = -1;
    }
    else
    {
        $to = 1;
    }

    $t_hour = substr($time_zone, 1, 2) * $to;
    $t_min = substr($time_zone, 3, 2) * $to;

    $server_time_zone = date("O");
    if($server_time_zone < 0)
    {
        $so = -1;
    }
    else
    {
        $so = 1;
    }

    $c_hour = substr($server_time_zone, 1, 2) * $so;
    $c_min = substr($server_time_zone, 3, 2) * $so;

    $g_min = $t_min - $c_min;
    $g_hour = $t_hour - $c_hour;

    $gap = $g_min * 60 + $g_hour * 60 * 60;
    return $gap;
}

/**
 * Trim a given number to a fiven size recursively
 *
 * @param int $no A given number
 * @param int $size A given digits
 */
function getNumberingPath($no, $size = 3)
{
    $mod = pow(10, $size);
    $output = sprintf('%0' . $size . 'd/', $no % $mod);
    if($no >= $mod)
    {
        $output .= getNumberingPath((int) $no / $mod, $size);
    }
    return $output;
}

/**
 * Decode the URL in Korean
 *
 * @param string $str The url
 * @return string
 **/
function url_decode($str)
{
    return preg_replace('/%u([[:alnum:]]{4})/', '&#x\\1;', $str);
}

/**
 * Return the requested script path
 *
 * @return string
 */
function getScriptPath()
{
    return _X_RELATIVE_PATH_;
}

/**
 * Return the requested script path
 *
 * @return string
 */
function getRequestUriByServerEnviroment()
{
    return str_replace('<', '&lt;', $_SERVER['REQUEST_URI']);
}

/**
 * PHP unescape function of javascript's escape
 * Function converts an Javascript escaped string back into a string with specified charset (default is UTF-8).
 * Modified function from http://pure-essence.net/stuff/code/utf8RawUrlDecode.phps
 *
 * @param string $source
 * @return string
 */
function utf8RawUrlDecode($source)
{
    $decodedStr = '';
    $pos = 0;
    $len = strlen($source);
    while($pos < $len)
    {
        $charAt = substr($source, $pos, 1);
        if($charAt == '%')
        {
            $pos++;
            $charAt = substr($source, $pos, 1);
            if($charAt == 'u')
            {
                // we got a unicode character
                $pos++;
                $unicodeHexVal = substr($source, $pos, 4);
                $unicode = hexdec($unicodeHexVal);
                $decodedStr .= _code2utf($unicode);
                $pos += 4;
            }
            else
            {
                // we have an escaped ascii character
                $hexVal = substr($source, $pos, 2);
                $decodedStr .= chr(hexdec($hexVal));
                $pos += 2;
            }
        }
        else
        {
            $decodedStr .= $charAt;
            $pos++;
        }
    }
    return $decodedStr;
}

/**
 * Returns utf-8 string of given code
 *
 * @param int $num
 * @return string
 */
function _code2utf($num)
{
    if($num < 128)
    {
        return chr($num);
    }
    if($num < 2048)
    {
        return chr(($num >> 6) + 192) . chr(($num & 63) + 128);
    }
    if($num < 65536)
    {
        return chr(($num >> 12) + 224) . chr((($num >> 6) & 63) + 128) . chr(($num & 63) + 128);
    }
    if($num < 2097152)
    {
        return chr(($num >> 18) + 240) . chr((($num >> 12) & 63) + 128) . chr((($num >> 6) & 63) + 128) . chr(($num & 63) + 128);
    }
    return '';
}

/**
 * Get whether utf8 or not given string
 *
 * @param string $string
 * @param bool $return_convert If set, returns converted string
 * @param bool $urldecode
 * @return bool|string
 */
function detectUTF8($string, $return_convert = FALSE, $urldecode = TRUE)
{
    if($urldecode)
    {
        $string = urldecode($string);
    }

    $sample = iconv('utf-8', 'utf-8', $string);
    $is_utf8 = (md5($sample) == md5($string));

    if(!$urldecode)
    {
        $string = urldecode($string);
    }

    if($return_convert)
    {
        return ($is_utf8) ? $string : iconv('euc-kr', 'utf-8', $string);
    }

    return $is_utf8;
}

/**
 * get json encoded string of data
 *
 * @param mixed $data
 * @return string
 */
function json_encode2($data)
{
    switch (gettype($data)) {
        case 'boolean':
            return $data ? 'true' : 'false';
        case 'integer':
        case 'double':
            return $data;
        case 'string':
            return '"' . strtr($data, array('\\' => '\\\\', '"' => '\\"')) . '"';
        case 'object':
            $data = get_object_vars($data);
        case 'array':
            $rel = false; // relative array?
            $key = array_keys($data);
            foreach ($key as $v) {
                if (!is_int($v)) {
                    $rel = true;
                    break;
                }
            }

            $arr = array();
            foreach ($data as $k => $v) {
                $arr[] = ($rel ? '"' . strtr($k, array('\\' => '\\\\', '"' => '\\"')) . '":' : '') . json_encode2($v);
            }

            return $rel ? '{' . join(',', $arr) . '}' : '[' . join(',', $arr) . ']';
        default:
            return '""';
    }
}


function array_clone($array)
{
    return array_merge(array(), $array);
}

/**
 * Print raw html header
 *
 * @return void
 */
function htmlHeader()
{
    echo '<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8" />
</head>
<body>';
}

/**
 * Print raw html footer
 *
 * @return void
 */
function htmlFooter()
{
    echo '</body></html>';
}

/**
 * Print raw alert message script
 *
 * @param string $msg
 * @return void
 */
function alertScript($msg)
{
    if (!$msg) {
        return;
    }

    echo '<script type="text/javascript">
//<![CDATA[
alert("' . $msg . '");
//]]>
</script>';
}

/**
 * Print raw close window script
 *
 * @return void
 */
function closePopupScript()
{
    echo '<script type="text/javascript">
//<![CDATA[
window.close();
//]]>
</script>';
}

/**
 * Print raw reload script
 *
 * @param bool $isOpener
 * @return void
 */
function reload($isOpener = FALSE)
{
    $reloadScript = $isOpener ? 'window.opener.location.reload()' : 'document.location.reload()';

    echo '<script type="text/javascript">
//<![CDATA[
' . $reloadScript . '
//]]>
</script>';
}