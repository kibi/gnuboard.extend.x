<?php
if(version_compare(PHP_VERSION, '5.4.0', '<'))
{
    @error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED ^ E_WARNING);
}
else
{
    @error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED ^ E_WARNING ^ E_STRICT);
}

if(!defined('__X__'))
{
    exit();
}

/**
 * The base path
 */
define('_X_PATH_', str_replace('config/config.inc.php', '', str_replace('\\', '/', __FILE__)));
define('_X_RELATIVE_PATH_', str_replace($_SERVER['DOCUMENT_ROOT'], '', _X_PATH_));
define('_X_VERSION_', '0.1.0');

require_once _X_PATH_ . 'config/func.inc.php';

require_once _X_PATH_ . 'classes/object/Object.class.php';
require_once _X_PATH_ . 'classes/xml/XmlParser.class.php';
require_once _X_PATH_ . 'classes/context/Context.class.php';
require_once _X_PATH_ . 'classes/file/FileHandler.class.php';
require_once _X_PATH_ . 'classes/frontendfile/FrontEndFileHandler.class.php';
require_once _X_PATH_ . 'classes/display/DisplayHandler.class.php';
require_once _X_PATH_ . 'classes/cache/CacheHandler.class.php';
require_once _X_PATH_ . 'classes/template/TemplateHandler.class.php';
require_once _X_PATH_ . 'classes/db/DB.class.php';
require_once _X_PATH_ . 'classes/db/Query.class.php';
require_once _X_PATH_ . 'classes/module/ModuleHandler.class.php';
require_once _X_PATH_ . 'classes/module/ModuleObject.class.php';
require_once _X_PATH_ . 'classes/page/PageHandler.class.php';