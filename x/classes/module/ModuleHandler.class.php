<?php

class ModuleHandler
{
    var $module = null;
    var $act = null;
    var $error = null;
    var $httpStatusCode = null;

    function ModuleHandler($module = '', $act = '')
    {

        $oContext = Context::getInstance();
        if (!$oContext->isSuccessInit()) {
            $this->error = 'msg_invalid_request';
            return;
        }

        $this->module = $module ? $module : Context::get('module');
        $this->act = $act ? $act : Context::get('act');

        // Validate variables to prevent XSS
        $isInvalid = NULL;
        if (!$this->module || !$this->act) {
            $isInvalid = TRUE;
        }
        if ($this->module && !preg_match("/^([a-z0-9\_\-]+)$/i", $this->module)) {
            $isInvalid = TRUE;
        }
        if ($this->act && !preg_match("/^([a-z0-9\_\-]+)$/i", $this->act)) {
            $isInvalid = TRUE;
        }

        if ($isInvalid) {
            $this->error = 'msg_invalid_request';
            return;
        }
    }

    function init()
    {
        return true;
    }

    function &procModule()
    {
        // If error occurred while preparation, return a message instance
        if ($this->error) {
            return $this->displayMessage(-1, $this->error, $this->error, $this->httpStatusCode);
        }

        // still no act means error
        if (!$this->act) {
            $this->error = 'msg_module_is_not_exists';
            $this->httpStatusCode = '404';
            return $this->displayMessage(-1, $this->error, $this->error, $this->httpStatusCode);
        }

        $type = null;
        $kind = 'svc';
        if (preg_match('/^([a-z]+)([A-Z][a-z0-9\_]+)(Admin)?(.*)$/', $this->act, $matches)) {
            $prefix = $matches[1];
            $admin = $matches[3];
            switch ($prefix) {
                case 'proc':
                    $type = 'controller';
                    break;
                case 'disp':
                    $type = 'view';
                    break;
            }
            if ($admin) {
                $kind = 'admin';
            }
        }

        if ($type == null) {
            $this->error = 'msg_invalid_request';
            return $this->displayMessage(-1, $this->error, $this->error);
        }

        $oModule = ModuleHandler::getModuleInstance($this->module, $type, $kind);

        if (!is_object($oModule)) {
            $this->error = 'msg_module_is_not_exists';
            $this->httpStatusCode = '404';
            return $this->displayMessage(-1, $this->error, $this->error, $this->httpStatusCode);
        }

        if (method_exists($oModule, 'init')) {
            $oModule->init();
        }

        $oModule->setAct($this->act);
        if (!$oModule->proc()) {
            return $this->displayMessage($oModule->getError(), $oModule->getMessage(), $oModule->getOriginMessage());
        } else {
            if ($type == 'controller' && Context::getResponseMethod() == 'HTML') {
                if ($oModule->getRedirectUrl()) {
                    header(sprintf('Location: %s', htmlspecialchars_decode($oModule->getRedirectUrl())));
                    exit;
                } else {
                    return $this->displayMessage($oModule->getError(), $oModule->getMessage(), $oModule->getOriginMessage());
                }
            }
        }

        return $oModule;
    }


    /**
     * returns module's path
     * @param string $module module name
     * @return string path of the module
     **/
    function getModulePath($module)
    {
        return sprintf('./modules/%s/', $module);
    }

    /**
     * It creates a module instance
     * @param string $module module name
     * @param string $type instance type, (e.g., view, controller, model)
     * @param string $kind admin or svc
     * @return ModuleObject module instance (if failed it returns null)
     * @remarks if there exists a module instance created before, returns it.
     **/
    function &getModuleInstance($module, $type = 'controller', $kind = '')
    {
        $kind = strtolower($kind);
        $type = strtolower($type);

        $kinds = array('svc' => 1, 'admin' => 1);
        if (!isset($kinds[$kind])) $kind = 'svc';

        // if there is no instance of the module in global variable, create a new one
        if (!isset($GLOBALS['_loaded_module'][$module][$type][$kind])) {
            ModuleHandler::_getModuleFilePath($module, $type, $kind, $class_path, $high_class_file, $class_file, $instance_name);

            // Get base class name and load the file contains it
            if (!class_exists($module)) {
                $high_class_file = sprintf('%s%s%s.class.php', _X_PATH_, $class_path, $module);

                if (!file_exists($high_class_file)) return NULL;
                require_once($high_class_file);
            }

            // Get the name of the class file
            if (!is_readable($class_file)) return NULL;

            // Create an instance with eval function
            require_once($class_file);
            if (!class_exists($instance_name)) return NULL;
            $tmp_fn = create_function('', "return new {$instance_name}();");

            $oModule = $tmp_fn();
            if (!is_object($oModule)) return NULL;

            // Set variables to the instance
            $oModule->setModule($module);
            $oModule->setModulePath($class_path);
            // If the module has a constructor, run it.
            if (!isset($GLOBALS['_called_constructor'][$instance_name])) {
                $GLOBALS['_called_constructor'][$instance_name] = true;
                if (@method_exists($oModule, $instance_name)) $oModule->{$instance_name}();
            }

            // Store the created instance into GLOBALS variable
            $GLOBALS['_loaded_module'][$module][$type][$kind] = $oModule;
        }

        // return the instance
        return $GLOBALS['_loaded_module'][$module][$type][$kind];
    }

    /**
     * display contents from executed module
     * @param ModuleObject $oModule module instance
     * @return void
     * */
    function displayContent($oModule = NULL)
    {
        // If the module is not set or not an object, set error
        if (!$oModule || !is_object($oModule)) {
            $this->error = 'msg_module_is_not_exists';
            $this->httpStatusCode = '404';
        }

        // Use message view object, if HTML call
        $methodList = array('XMLRPC' => 1, 'JSON' => 1, 'JS_CALLBACK' => 1);
        if (!isset($methodList[Context::getRequestMethod()])) {
            // If error occurred, handle it
            if ($this->error) {
                // display content with message module instance
                $oModule = $this->displayMessage(-1, $this->error, $this->httpStatusCode);

                if ($oModule->getHttpStatusCode() && $oModule->getHttpStatusCode() != '200') {
                    $this->_setHttpStatusMessage($oModule->getHttpStatusCode());
                    $oModule->setTemplateFile('http_status_code');
                }
            }
        }

        // Display contents
        $oDisplayHandler = new DisplayHandler();
        $oDisplayHandler->printContent($oModule);
    }

    function displayMessage($code, $message, $origin_message=null, $status_code = null)
    {
        $oMessageObject = ModuleHandler::getModuleInstance('message', 'view');
        $oMessageObject->setError($code);
        $oMessageObject->setMessage($message);
        $oMessageObject->setOriginMessage($origin_message ? $origin_message : $message);
        $oMessageObject->dispMessage();
        if ($status_code) {
            $oMessageObject->setHttpStatusCode($status_code);
        }
        return $oMessageObject;
    }

    function _getModuleFilePath($module, $type, $kind, &$classPath, &$highClassFile, &$classFile, &$instanceName)
    {
        $classPath = ModuleHandler::getModulePath($module);

        $highClassFile = sprintf('%s%s%s.class.php', _X_PATH_, $classPath, $module);
        $highClassFile = FileHandler::getRealPath($highClassFile);

        $types = array('view', 'controller', 'model', 'api', 'class', 'extend');
        if (!in_array($type, $types)) $type = $types[0];
        if ($type == 'class') {
            $instanceName = '%s';
            $classFile = '%s%s.%s.php';
        } elseif ($kind == 'admin' && array_search($type, $types) < 3) {
            $instanceName = '%sAdmin%s';
            $classFile = '%s%s.admin.%s.php';
        } else {
            $instanceName = '%s%s';
            $classFile = '%s%s.%s.php';
        }
        $instanceName = sprintf($instanceName, $module, ucfirst($type));

        $classFile = sprintf($classFile, $classPath, strtolower($module), $type);
        $classFile = FileHandler::getRealPath($classFile);
    }

    /**
     * get http status message by http status code
     * @param string $code
     * @return string
     * */
    function _setHttpStatusMessage($code)
    {
        $statusMessageList = array(
            '100' => 'Continue',
            '101' => 'Switching Protocols',
            '201' => 'OK', // todo check array key '201'
            '201' => 'Created',
            '202' => 'Accepted',
            '203' => 'Non-Authoritative Information',
            '204' => 'No Content',
            '205' => 'Reset Content',
            '206' => 'Partial Content',
            '300' => 'Multiple Choices',
            '301' => 'Moved Permanently',
            '302' => 'Found',
            '303' => 'See Other',
            '304' => 'Not Modified',
            '305' => 'Use Proxy',
            '307' => 'Temporary Redirect',
            '400' => 'Bad Request',
            '401' => 'Unauthorized',
            '402' => 'Payment Required',
            '403' => 'Forbidden',
            '404' => 'Not Found',
            '405' => 'Method Not Allowed',
            '406' => 'Not Acceptable',
            '407' => 'Proxy Authentication Required',
            '408' => 'Request Timeout',
            '409' => 'Conflict',
            '410' => 'Gone',
            '411' => 'Length Required',
            '412' => 'Precondition Failed',
            '413' => 'Request Entity Too Large',
            '414' => 'Request-URI Too Long',
            '415' => 'Unsupported Media Type',
            '416' => 'Requested Range Not Satisfiable',
            '417' => 'Expectation Failed',
            '500' => 'Internal Server Error',
            '501' => 'Not Implemented',
            '502' => 'Bad Gateway',
            '503' => 'Service Unavailable',
            '504' => 'Gateway Timeout',
            '505' => 'HTTP Version Not Supported',
        );
        $statusMessage = $statusMessageList[$code];
        if (!$statusMessage) {
            $statusMessage = 'OK';
        }

        Context::set('http_status_code', $code);
        Context::set('http_status_message', $statusMessage);
    }
}