<?php

class ModuleObject extends Object
{

    var $module = NULL; ///< Class name of Xe Module that is identified by mid
    var $module_path = NULL; ///< a path to directory where module source code resides
    var $act = NULL; ///< a string value to contain the action name
    var $template_path = NULL; ///< a path of directory where template files reside
    var $template_file = NULL; ///< name of template file
    var $stop_proc = false; ///< a flag to indicating whether to stop the execution of code.
    var $ajaxRequestMethod = array('XMLRPC', 'JSON');

    /**
     * setter to set the name of module
     * @param string $module name of module
     * @return void
     * */
    function setModule($module)
    {
        $this->module = $module;
    }

    /**
     * setter to set the name of module path
     * @param string $path the directory path to a module directory
     * @return void
     * */
    function setModulePath($path)
    {
        if (substr_compare($path, '/', -1) !== 0) {
            $path .= '/';
        }
        $this->module_path = $path;
    }

    /**
     * setter to set an url for redirection
     * @param string $url url for redirection
     * @remark redirect_url is used only for ajax requests
     * @return void
     * */
    function setRedirectUrl($url = './', $output = NULL)
    {
        $ajaxRequestMethod = array_flip($this->ajaxRequestMethod);
        if(!isset($ajaxRequestMethod[Context::getRequestMethod()]))
        {
            $this->add('redirect_url', $url);
        }

        if($output !== NULL && is_object($output))
        {
            return $output;
        }
    }

    /**
     * get url for redirection
     * @return string redirect_url
     * */
    function getRedirectUrl()
    {
        return $this->get('redirect_url');
    }

    /**
     * set message
     * @param string $message a message string
     * @param string $type type of message (error, info, update)
     * @return void
     * */
    function setMessage($message, $type = NULL)
    {
        parent::setMessage($message);
        $this->setMessageType($type);
    }

    /**
     * set type of message
     * @param string $type type of message (error, info, update)
     * @return void
     * */
    function setMessageType($type)
    {
        $this->add('message_type', $type);
    }

    /**
     * get type of message
     * @return string $type
     * */
    function getMessageType()
    {
        $type = $this->get('message_type');
        $typeList = array('error' => 1, 'info' => 1, 'update' => 1);
        if(!isset($typeList[$type]))
        {
            $type = $this->getError() ? 'error' : 'info';
        }
        return $type;
    }

    /**
     * sett to set the template path for refresh.html
     * refresh.html is executed as a result of method execution
     * Tpl as the common run of the refresh.html ..
     * @return void
     * */
    function setRefreshPage()
    {
        $this->setTemplatePath('./common/tpl');
        $this->setTemplateFile('refresh');
    }

    /**
     * sett to set the action name
     * @param string $act
     * @return void
     * */
    function setAct($act)
    {
        $this->act = $act;
    }

    /**
     * set the stop_proc and approprate message for msg_code
     * @param string $msg_code an error code
     * @return ModuleObject $this
     * */
    function stop($msg_code)
    {
        // flag setting to stop the proc processing
        $this->stop_proc = TRUE;
        // Error handling
        $this->setError(-1);
        $this->setMessage($msg_code);
        // Error message display by message module
        $type = 'view';
        $oMessageObject = ModuleHandler::getModuleInstance('message', $type);
        $oMessageObject->setError(-1);
        $oMessageObject->setMessage($msg_code);
        $oMessageObject->dispMessage();

        $this->setTemplatePath($oMessageObject->getTemplatePath());
        $this->setTemplateFile($oMessageObject->getTemplateFile());

        return $this;
    }

    /**
     * set the file name of the template file
     * @param string name of file
     * @return void
     * */
    function setTemplateFile($filename)
    {
        if (isset($filename) && substr_compare($filename, '.html', -5) !== 0) {
            $filename .= '.html';
        }
        $this->template_file = $filename;
    }

    /**
     * retrieve the directory path of the template directory
     * @return string
     * */
    function getTemplateFile()
    {
        return $this->template_file;
    }

    /**
     * set the directory path of the template directory
     * @param string path of template directory.
     * @return void
     * */
    function setTemplatePath($path)
    {
        if (!$path) return;

        if ((strlen($path) >= 1 && substr_compare($path, '/', 0, 1) !== 0) && (strlen($path) >= 2 && substr_compare($path, './', 0, 2) !== 0)) {
            $path = './' . $path;
        }

        if (substr_compare($path, '/', -1) !== 0) {
            $path .= '/';
        }
        $this->template_path = $path;
    }

    /**
     * retrieve the directory path of the template directory
     * @return string
     * */
    function getTemplatePath()
    {
        return $this->template_path;
    }

    /**
     * excute the method specified by $act variable
     * @return boolean true : success false : fail
     * */
    function proc()
    {
        // pass if stop_proc is true
        if ($this->stop_proc) {
            return false;
        }

        Context::set('module_info', $this);

        if (method_exists($this, $this->act)) {
            // Run
            $output = $this->{$this->act}();
        } else {
            $this->stop("msg_not_found_act");
            return false;
        }

        if (is_a($output, 'Object') || is_subclass_of($output, 'Object')) {
            $this->setError($output->getError());
            $this->setMessage($output->getMessage());
            $this->setOriginMessage($output->getOriginMessage());

            if (!$output->toBool()) {
                return false;
            }
        }
        return true;
    }
}