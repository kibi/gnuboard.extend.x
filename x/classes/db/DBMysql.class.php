<?php
/**
 * Class to use MySQL DBMS
 * mysql handling class
 *
 * Does not use prepared statements, since mysql driver does not support them
 */
class DBMysql extends DB
{
    /**
     * Constructor
     * @return void
     */
    function DBMysql()
    {
        $this->_setDBInfo();
        $this->_connect();
    }

    /**
     * Create an instance of this class
     * @return DBMysql return DBMysql object instance
     */
    function create()
    {
        return new DBMysql;
    }

    /**
     * DB Connect
     * this method is private
     * @param array $connection connection's value is db_hostname, db_port, db_database, db_userid, db_password
     * @return resource
     */
    function __connect($connection)
    {
        // Ignore if no DB information exists
        if(strpos($connection["db_hostname"], ':') === false && $connection["db_port"])
        {
            $connection["db_hostname"] .= ':' . $connection["db_port"];
        }

        // Attempt to connect
        $result = @mysql_connect($connection["db_hostname"], $connection["db_userid"], $connection["db_password"]);
        if(!$result)
        {
            exit('cannot connect to DB.');
        }

        if(mysql_error())
        {
            $this->setError(mysql_errno(), mysql_error());
            return;
        }
        // Error appears if the version is lower than 4.1
        if(mysql_get_server_info($result) < "4.1")
        {
            $this->setError(-1, "cannot be installed under the version of mysql 4.1. Current mysql version is " . mysql_get_server_info());
            return;
        }
        // select db
        @mysql_select_db($connection["db_database"], $result);
        if(mysql_error())
        {
            $this->setError(mysql_errno(), mysql_error());
            return;
        }

        return $result;
    }

    /**
     * If have a task after connection, add a taks in this method
     * this method is private
     * @param resource $connection
     * @return void
     */
    function _afterConnect($connection)
    {
        // Set utf8 if a database is MySQL
        $this->_query("set names 'utf8'", $connection);
    }

    /**
     * DB disconnection
     * this method is private
     * @param resource $connection
     * @return void
     */
    function _close($connection)
    {
        @mysql_close($connection);
    }

    /**
     * Handles quatation of the string variables from the query
     * @param string $string
     * @return string
     */
    function addQuotes($string)
    {
        if(version_compare(PHP_VERSION, "5.9.0", "<") && get_magic_quotes_gpc())
        {
            $string = stripslashes(str_replace("\\", "\\\\", $string));
        }
        if(!is_numeric($string))
        {
            $string = @mysql_real_escape_string($string);
        }
        return $string;
    }

    /**
     * DB transaction start
     * this method is private
     * @return boolean
     */
    function _begin()
    {
        return true;
    }

    /**
     * DB transaction rollback
     * this method is private
     * @return boolean
     */
    function _rollback()
    {
        return true;
    }

    /**
     * DB transaction commit
     * this method is private
     * @return boolean
     */
    function _commit()
    {
        return true;
    }

    /**
     * Execute the query
     * this method is private
     * @param string $query
     * @param resource $connection
     * @return resource
     */
    function __query($query, $connection)
    {
        $this->setError(0);

        // Run the query statement
        $result = mysql_query($query, $connection);
        // Error Check
        if(mysql_error($connection))
        {
            $this->setError(mysql_errno($connection), mysql_error($connection));
        }

        // Return result
        return $result;
    }

    /**
     * Fetch the result
     * @param resource $result
     * @param int|NULL $arrayIndexEndValue
     * @return array
     */
    function _fetch($result, $arrayIndexEndValue = NULL)
    {
        $output = null;
        if(!$this->isConnected() || $this->isError() || !$result)
        {
            return $output;
        }
        while($tmp = $this->db_fetch_object($result))
        {
            if(!isset($output)) $output = array();

            if($arrayIndexEndValue)
            {
                $output[$arrayIndexEndValue--] = $tmp;
            }
            else
            {
                $output[] = $tmp;
            }
        }
        if(count($output) == 1)
        {
            if(isset($arrayIndexEndValue))
            {
                return $output;
            }
            else
            {
                return $output[0];
            }
        }
        $this->db_free_result($result);
        return $output;
    }

    function getNextAutoIncrement($table_name)
    {
        $result = $this->_query("SHOW TABLE STATUS LIKE '{$table_name}'");
        if($this->isError())
        {
            return 0;
        }

        $tmp = $this->_fetch($result);

        if (!$tmp) return 0;

        return $tmp->Auto_increment;
    }

    /**
     * Check a table exists status
     * @param string $target_name
     * @return boolean
     */
    function isTableExists($target_name)
    {
        $query = sprintf("show tables like '%s'", $this->addQuotes($target_name));
        $result = $this->_query($query);
        $tmp = $this->_fetch($result);
        if(!$tmp)
        {
            return false;
        }
        return true;
    }

    /**
     * Drop tables
     * @param string $table_name
     * @return void
     */
    function dropTable($table_name)
    {
        if(!$table_name)
        {
            return;
        }
        $query = sprintf("drop table %s", $table_name);
        $this->_query($query);
    }

    /**
     * Add a column to the table
     * @param string $table_name table name
     * @param string $column_name column name
     * @param string $type column type, default value is 'number'
     * @param int $size column size
     * @param string|int $default default value
     * @param boolean $notnull not null status, default value is false
     * @return void
     */
    function addColumn($table_name, $column_name, $type = 'integer', $size = '', $default = '', $notnull = false)
    {
        if(strtoupper($type) == 'INTEGER')
        {
            $size = '';
        }

        $query = sprintf("alter table `%s` add `%s` ", $table_name, $column_name);
        if($size)
        {
            $query .= sprintf(" %s(%s) ", $type, $size);
        }
        else
        {
            $query .= sprintf(" %s ", $type);
        }
        if($default)
        {
            $query .= sprintf(" default '%s' ", $default);
        }
        if($notnull)
        {
            $query .= " not null ";
        }

        return $this->_query($query);
    }

    /**
     * Drop a column from the table
     * @param string $table_name table name
     * @param string $column_name column name
     * @return void
     */
    function dropColumn($table_name, $column_name)
    {
        $query = sprintf("alter table `%s` drop `%s` ", $table_name, $column_name);
        $this->_query($query);
    }

    /**
     * Check column exist status of the table
     * @param string $table_name table name
     * @param string $column_name column name
     * @return boolean
     */
    function isColumnExists($table_name, $column_name)
    {
        if (func_num_args() > 2) {
            $args = func_get_args();
            array_shift($args);
            $column_name = $args;
        }

        if (is_string($column_name)) {
            $column_name = array($column_name);
        }

        if (is_array($column_name)) {
            $condition = sprintf('where Field in ("%s")', implode('", "', $column_name));
        }

        $query = sprintf("show fields from `%s` %s", $table_name, $condition);
        $result = $this->_query($query);
        if($this->isError())
        {
            return false;
        }
        $output = $this->_fetch($result);

        if (count($output) == count($column_name)) return true;
        return false;
    }

    /**
     * Add an index to the table
     * $target_columns = array(col1, col2)
     * $is_unique? unique : none
     * @param string $table_name table name
     * @param string $index_name index name
     * @param string|array $target_columns target column or columns
     * @param boolean $is_unique
     * @return void
     */
    function addIndex($table_name, $index_name, $target_columns, $is_unique = false)
    {
        if(!is_array($target_columns))
        {
            $target_columns = array($target_columns);
        }

        $query = sprintf("alter table `%s` add %s index `%s` (%s);", $table_name, $is_unique ? 'unique' : '', $index_name, implode(',', $target_columns));
        $this->_query($query);
    }

    /**
     * Drop an index from the table
     * @param string $table_name table name
     * @param string $index_name index name
     * @param boolean $is_unique
     * @return void
     */
    function dropIndex($table_name, $index_name, $is_unique = false)
    {
        $query = sprintf("alter table `%s` drop index `%s`", $table_name, $index_name);
        $this->_query($query);
    }

    /**
     * Check index status of the table
     * @param string $table_name table name
     * @param string $index_name index name
     * @return boolean
     */
    function isIndexExists($table_name, $index_name)
    {
        $query = sprintf("show indexes from `%s`", $table_name);
        $result = $this->_query($query);
        if($this->isError())
        {
            return;
        }
        $output = $this->_fetch($result);
        if(!$output)
        {
            return;
        }
        if(!is_array($output))
        {
            $output = array($output);
        }

        for($i = 0; $i < count($output); $i++)
        {
            if($output[$i]->Key_name == $index_name)
            {
                return true;
            }
        }
        return false;
    }

    function isProcedureExists($procedureName){
        $output = executeQuery("SHOW PROCEDURE STATUS LIKE '{$procedureName}'");
        return $output->data ? true : false;
    }

    /**
     * Get the ID generated in the last query
     * Return next sequence from sequence table
     * This method use only mysql
     * @return int
     */
    function db_insert_id()
    {
        $connection = $this->_getConnection('master');
        return mysql_insert_id($connection);
    }

    /**
     * Fetch a result row as an object
     * @param resource $result
     * @return object
     */
    function db_fetch_object(&$result)
    {
        return mysql_fetch_object($result);
    }

    /**
     * Free result memory
     * @param resource $result
     * @return boolean Returns TRUE on success or FALSE on failure.
     */
    function db_free_result(&$result)
    {
        return mysql_free_result($result);
    }
}

DBMysql::$isSupported = function_exists('mysql_connect');