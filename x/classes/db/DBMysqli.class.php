<?php
require_once('DBMysql.class.php');

/**
 * Class to use MySQLi DBMS as mysqli_*
 * mysql handling class
 *
 * Does not use prepared statements, since mysql driver does not support them
 */
class DBMysqli extends DBMysql
{

    /**
     * Constructor
     * @return void
     */
    function DBMysqli()
    {
        $this->_setDBInfo();
        $this->_connect();
    }

    /**
     * Create an instance of this class
     * @return DBMysqli return DBMysqli object instance
     */
    function create()
    {
        return new DBMysqli;
    }

    /**
     * DB Connect
     * this method is private
     * @param array $connection connection's value is db_hostname, db_port, db_database, db_userid, db_password
     * @return resource
     */
    function __connect($connection)
    {
        // Attempt to connect
        if($connection["db_port"])
        {
            $result = @mysqli_connect($connection["db_hostname"]
                , $connection["db_userid"]
                , $connection["db_password"]
                , $connection["db_database"]
                , $connection["db_port"]);
        }
        else
        {
            $result = @mysqli_connect($connection["db_hostname"]
                , $connection["db_userid"]
                , $connection["db_password"]
                , $connection["db_database"]);
        }
        $error = mysqli_connect_errno();
        if($error)
        {
            $this->setError($error, mysqli_connect_error());
            return;
        }
        mysqli_set_charset($result, 'utf8');
        return $result;
    }

    /**
     * DB disconnection
     * this method is private
     * @param resource $connection
     * @return void
     */
    function _close($connection)
    {
        mysqli_close($connection);
    }

    /**
     * DB transaction start
     * this method is private
     * @return boolean
     */
    function _begin($transactionLevel)
    {
        $connection = $this->_getConnection('master');

        if(!$transactionLevel)
        {
            $this->_query("begin");
        }
        else
        {
            $this->_query("SAVEPOINT SP" . $transactionLevel, $connection);
        }
        return true;
    }

    /**
     * DB transaction rollback
     * this method is private
     * @return boolean
     */
    function _rollback($transactionLevel)
    {
        $connection = $this->_getConnection('master');

        $point = $transactionLevel - 1;

        if($point)
        {
            $this->_query("ROLLBACK TO SP" . $point, $connection);
        }
        else
        {
            mysqli_rollback($connection);
        }
        return true;
    }

    /**
     * DB transaction commit
     * this method is private
     * @return boolean
     */
    function _commit()
    {
        $connection = $this->_getConnection('master');
        mysqli_commit($connection);
        return true;
    }

    /**
     * Handles quatation of the string variables from the query
     * @param string $string
     * @return string
     */
    function addQuotes($string)
    {
        if(version_compare(PHP_VERSION, "5.9.0", "<") && get_magic_quotes_gpc())
        {
            $string = stripslashes(str_replace("\\", "\\\\", $string));
        }
        if(!is_numeric($string))
        {
            $connection = $this->_getConnection('master');
            $string = mysqli_escape_string($connection, $string);
        }
        return $string;
    }

    /**
     * Execute the query
     * this method is private
     * @param string $query
     * @param resource $connection
     * @return resource
     */
    function __query($query, $connection)
    {
        $this->setError(0);

        // Run the query statement
        $result = mysqli_query($connection, $query);
        // Error Check
        $error = mysqli_error($connection);
        if($error)
        {
            $this->setError(mysqli_errno($connection), $error);
        }
        // Return result
        return $result;
    }

    /**
     * Get the ID generated in the last query
     * Return next sequence from sequence table
     * This method use only mysql
     * @return int
     */
    function db_insert_id()
    {
        $connection = $this->_getConnection('master');
        return mysqli_insert_id($connection);
    }

    /**
     * Fetch a result row as an object
     * @param resource $result
     * @return object
     */
    function db_fetch_object(&$result)
    {
        return mysqli_fetch_object($result);
    }

    /**
     * Free result memory
     * @param resource $result
     * @return boolean Returns TRUE on success or FALSE on failure.
     */
    function db_free_result(&$result)
    {
        return mysqli_free_result($result);
    }

}

DBMysqli::$isSupported = function_exists('mysqli_connect');
