<?php

class Query
{
    var $columns = array();
    var $orders = array();
    var $listCount = 0;
    var $pageCount = 10;
    var $page = 1;

    function Query($options = null)
    {
        if (func_num_args() > 1) {
            $args = func_get_args();
            $args_count = count($args);
            $options = new stdClass();

            switch ($args_count) {
                case 5:
                    $pageCount = $args[4];
                    $options->pageCount = $pageCount;
                case 4:
                    $listCount = $args[3];
                    $options->listCount = $listCount;
                case 3:
                    $page = $args[2];
                    $options->page = $page;
                case 2:
                    $orders = $args[1];
                    if (is_string($orders)) $orders = array($orders);
                    $options->orders = $orders;
                case 1:
                    $columns = $args[0];
                    if (is_string($columns)) $columns = array($columns);
                    $options->columns = $columns;
                    break;
            }
        }

        if (is_array($options)) $options = (object)$options;

        if (isset($options)) {
            if (isset($options->columns)) {
                $this->columns = $options->columns;
            } else if (isset($options->column) && is_string($options->column)) {
                $this->columns = array($options->column);
            }

            if (isset($options->orders)) {
                $this->orders = $options->orders;
            } else if (isset($options->order) && is_string($options->order)) {
                $this->orders = array($options->order);
            }

            if ($options->listCount > 0) {
                $this->listCount = $options->listCount;
            }
            if ($options->pageCount > 0) {
                $this->pageCount = $options->pageCount;
            }
            if ($options->page > 0) {
                $this->page = $options->page;
            }
        }
    }

    function addColumn($column)
    {
        if (isset($column) || empty($column)) return;

        array_push($this->columns, $column);

        return $this;
    }

    function setLimit($offset = 0, $row_count = 10)
    {
        $this->offset = $offset;
        $this->row_count = $row_count;

        return $this;
    }

    function addOrderBy($order)
    {
        if (isset($order) || empty($order)) return;

        array_push($this->orders, $order);

        return $this;
    }

    /**
     * @param boolean $is_total
     */
    public function setRequireCount($is_total)
    {
        $this->require_count = $is_total;
    }


    function executeQuery($query)
    {
        $result = new Object();

        if ($this->listCount > 0) {
            $count_query = preg_replace('/(?<=select)(?(?=\()(\(([^()]|(?1))*\))|(?(?=from)[^from]|.))+/i', " count(*) as count ", $query, 1);

            $count_output = executeQuery($count_query);
            if (!$count_output->toBool()) return $count_output;
            if(is_array($count_output->data)){
                $total_count = count($count_output->data);
            }else{
                $total_count = (int)(isset($count_output->data->count) ? $count_output->data->count : 0);
            }
            $total_page = $total_count ? (int)(($total_count - 1) / $this->listCount) + 1 : 1;

            $result->navigation = new PageHandler($total_count, $total_page, $this->page, $this->pageCount);
            $result->navigation->list_count = $this->listCount;
        }

        if (is_string($this->columns)) $this->columns = array($this->columns);
        if (count($this->columns) > 0) {
            $columns = implode(', ', $this->columns);
        }

        if (is_string($this->orders)) $this->orders = array($this->orders);
        if (count($this->orders) > 0) {
            $orders = ' order by ' . implode(', ', $this->orders);
        }

        if ($this->listCount > 0) {
            $offset = ($this->page - 1) * $this->listCount;
            $limit = sprintf(' limit %d, %d', $offset, $this->listCount);
        }

        if ($columns) {
            $query = preg_replace('/(?<=select)(?(?=\()(\(([^()]|(?1))*\))|(?(?=from)[^from]|.))+/i', " {$columns} ", $query, 1);
        }
        if ($orders) {
            $query .= $orders;
        }
        if ($limit) {
            $query .= $limit;
        }

        $output = executeQuery($query);
        if (!$output->toBool()) return $output;
        $result->data = $output->data;

        return $result;
    }

    function executeQueryArray($query)
    {
        $output = $this->executeQuery($query);
        if (!is_array($output->data) && count($output->data) > 0) {
            $output->data = array($output->data);
        }else if(!isset($output->data)){
            $output->data = array();
        }

        return $output;
    }
}