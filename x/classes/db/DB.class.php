<?php

class DB
{

    static $isSupported = FALSE;

    /**
     * master database connection string
     * @var array
     */
    var $master_db = NULL;

    /**
     * array of slave databases connection strings
     * @var array
     */
    var $slave_db = NULL;

    /**
     * error code (0 means no error)
     * @var int
     */
    var $errno = 0;

    /**
     * error message
     * @var string
     */
    var $errstr = '';

    var $connection = '';

    /**
     * transaction flag
     * @var boolean
     */
    var $transaction_started = FALSE;
    var $is_connected = FALSE;

    /**
     * returns enable list in supported dbms list
     * will be written by classes/DB/DB***.class.php
     * @var array
     */
    var $supported_list = array();

    /**
     * stores database type: 'mysql','cubrid','mssql' etc. or 'db' when database is not yet set
     * @var string
     */
    var $db_type;

    /**
     * leve of transaction
     * @var unknown
     */
    private $transationNestedLevel = 0;

    /**
     * returns instance of certain db type
     * @param string $db_type type of db
     * @return DB return DB object instance
     */
    function &getInstance($db_type = NULL)
    {
        if (!$db_type) {
            $db_type = Context::getDBType();
        }
        if (!$db_type && Context::isInstalled()) {
            return new Object(-1, 'msg_db_not_setted');
        }

        if (!isset($GLOBALS['__DB__'])) {
            $GLOBALS['__DB__'] = array();
        }
        if (!isset($GLOBALS['__DB__'][$db_type])) {
            $class_name = 'DB' . ucfirst($db_type);
            $class_file = _X_PATH_ . "classes/db/$class_name.class.php";
            if (!file_exists($class_file)) {
                return new Object(-1, 'msg_db_not_setted');
            }

            // get a singletone instance of the database driver class
            require_once($class_file);
            $GLOBALS['__DB__'][$db_type] = call_user_func(array($class_name, 'create'));
            $GLOBALS['__DB__'][$db_type]->db_type = $db_type;
        }

        return $GLOBALS['__DB__'][$db_type];
    }

    /**
     * returns instance of db
     * @return DB return DB object instance
     */
    function create()
    {
        return new DB;
    }

    /**
     * constructor
     * @return void
     */
    function DB()
    {
    }

    /**
     * returns list of supported dbms list
     * this list return by directory list
     * check by instance can creatable
     * @return array return supported DBMS list
     */
    function getSupportedList()
    {
        $oDB = new DB();
        return $oDB->_getSupportedList();
    }

    /**
     * returns list of supported dbms list
     * this method is private
     * @return array return supported DBMS list
     */
    function _getSupportedList()
    {
        static $get_supported_list = '';
        if (is_array($get_supported_list)) {
            $this->supported_list = $get_supported_list;
            return $this->supported_list;
        }
        $get_supported_list = array();
        $db_classes_path = _X_PATH_ . "classes/db/";
        $filter = "/^DB([^\.]+)\.class\.php/i";
        $supported_list = FileHandler::readDir($db_classes_path, $filter, TRUE);

        // after creating instance of class, check is supported
        for ($i = 0; $i < count($supported_list); $i++) {
            $db_type = $supported_list[$i];

            if (version_compare(phpversion(), '5.0') < 0 && preg_match('/pdo/i', $db_type)) {
                continue;
            }

            $class_name = sprintf("DB%s%s", strtoupper(substr($db_type, 0, 1)), strtolower(substr($db_type, 1)));
            $class_file = sprintf(_X_PATH_ . "classes/db/%s.class.php", $class_name);
            if (!file_exists($class_file)) {
                continue;
            }

            unset($oDB);
            require_once($class_file);
            $tmp_fn = create_function('', "return new {$class_name}();");
            $oDB = $tmp_fn();

            if (!$oDB) {
                continue;
            }

            $obj = new stdClass;
            $obj->db_type = $db_type;
            $obj->enable = $oDB->isSupported() ? TRUE : FALSE;

            $get_supported_list[] = $obj;
        }

        $this->supported_list = $get_supported_list;
        return $this->supported_list;
    }

    /**
     * Return dbms supportable status
     * The value is set in the child class
     * @return boolean true: is supported, false: is not supported
     */
    function isSupported()
    {
        return self::$isSupported;
    }

    /**
     * Return connected status
     * @param string $type master or slave
     * @param int $indx key of server list
     * @return boolean true: connected, false: not connected
     */
    function isConnected($type = 'master', $indx = 0)
    {
        if ($type == 'master') {
            return $this->master_db["is_connected"] ? TRUE : FALSE;
        } else {
            return $this->slave_db[$indx]["is_connected"] ? TRUE : FALSE;
        }
    }

    /**
     * set error
     * @param int $errno error code
     * @param string $errstr error message
     * @return void
     */
    function setError($errno = 0, $errstr = 'success')
    {
        $this->errno = $errno;
        $this->errstr = $errstr;
    }

    /**
     * Return error status
     * @return boolean true: error, false: no error
     */
    function isError()
    {
        return ($this->errno !== 0);
    }

    /**
     * Returns object of error info
     * @return object object of error
     */
    function getError()
    {
        return new Object($this->errno, $this->errstr);
    }

    /**
     * Execute Query that result of the query xml file
     * This function finds xml file or cache file of $query_id, compiles it and then execute it
     * @param string $query_id query id (module.queryname)
     * @param array|object $args arguments for query
     * @param array $arg_columns column list. if you want get specific colums from executed result, add column list to $arg_columns
     * @return object result of query
     */
    function executeQuery($query)
    {
        $result = $this->_query($query);
        if ($this->isError()) return $this->getError();
        if ($result === true) return new Object();

        $output = new Object();
        $data = $this->_fetch($result);
        $output->data = $data;
        return $output;
    }

    /**
     * Return index from slave server list
     * @return int
     */
    function _getSlaveConnectionStringIndex()
    {
        $max = count($this->slave_db);
        $indx = rand(0, $max - 1);
        return $indx;
    }

    /**
     * Return connection resource
     * @param string $type use 'master' or 'slave'. default value is 'master'
     * @param int $indx if indx value is NULL, return rand number in slave server list
     * @return resource
     */
    function _getConnection($type = 'master', $indx = NULL)
    {
        if ($type == 'master') {
            if (!$this->master_db['is_connected']) {
                $this->_connect($type);
            }
            $this->connection = 'Master ' . $this->master_db['db_hostname'];
            return $this->master_db["resource"];
        }

        if ($indx === NULL) {
            $indx = $this->_getSlaveConnectionStringIndex($type);
        }

        if (!$this->slave_db[$indx]['is_connected']) {
            $this->_connect($type, $indx);
        }

        $this->connection = 'Slave ' . $this->slave_db[$indx]['db_hostname'];
        return $this->slave_db[$indx]["resource"];
    }

    /**
     * check db information exists
     * @return boolean
     */
    function _dbInfoExists()
    {
        if (!$this->master_db) {
            return FALSE;
        }
        if (count($this->slave_db) === 0) {
            return FALSE;
        }
        return TRUE;
    }

    /**
     * DB disconnection
     * this method is protected
     * @param resource $connection
     * @return void
     */
    function _close($connection)
    {

    }

    /**
     * DB disconnection
     * @param string $type 'master' or 'slave'
     * @param int $indx number in slave dbms server list
     * @return void
     */
    function close($type = 'master', $indx = 0)
    {
        if (!$this->isConnected($type, $indx)) {
            return;
        }

        if ($type == 'master') {
            $connection = & $this->master_db;
        } else {
            $connection = & $this->slave_db[$indx];
        }

        $this->commit();
        $this->_close($connection["resource"]);

        $connection["is_connected"] = FALSE;
    }

    /**
     * DB transaction start
     * this method is protected
     * @return boolean
     */
    function _begin()
    {
        return TRUE;
    }

    /**
     * DB transaction start
     * @return void
     */
    function begin()
    {
        if (!$this->isConnected()) {
            return;
        }

        if ($this->_begin($this->transationNestedLevel)) {
            $this->transaction_started = TRUE;
            $this->transationNestedLevel++;
        }
    }

    /**
     * DB transaction rollback
     * this method is protected
     * @return boolean
     */
    function _rollback()
    {
        return TRUE;
    }

    /**
     * DB transaction rollback
     * @return void
     */
    function rollback()
    {
        if (!$this->isConnected() || !$this->transaction_started) {
            return;
        }
        if ($this->_rollback($this->transationNestedLevel)) {
            $this->transationNestedLevel--;

            if (!$this->transationNestedLevel) {
                $this->transaction_started = FALSE;
            }
        }
    }

    /**
     * DB transaction commit
     * this method is protected
     * @return boolean
     */
    function _commit()
    {
        return TRUE;
    }

    /**
     * DB transaction commit
     * @param boolean $force regardless transaction start status or connect status, forced to commit
     * @return void
     */
    function commit($force = FALSE)
    {
        if (!$force && (!$this->isConnected() || !$this->transaction_started)) {
            return;
        }
        if ($this->transationNestedLevel == 1 && $this->_commit()) {
            $this->transaction_started = FALSE;
            $this->transationNestedLevel = 0;
        } else {
            $this->transationNestedLevel--;
        }
    }

    /**
     * Execute the query
     * this method is protected
     * @param string $query
     * @param resource $connection
     * @return void
     */
    function __query($query, $connection)
    {

    }

    /**
     * Execute the query
     * this method is protected
     * @param string $query
     * @param resource $connection
     * @return resource
     */
    function _query($query, $connection = NULL)
    {
        if ($connection == NULL) {
            $connection = $this->_getConnection('master');
        }

        // Run the query statement
        $result = $this->__query($query, $connection);

        // Return result
        return $result;
    }

    /**
     * DB info settings
     * this method is protected
     * @return void
     */
    function _setDBInfo()
    {
        $db_info = Context::getDBInfo();
        $this->master_db = $db_info->master_db;
        if ($db_info->master_db["db_hostname"] == $db_info->slave_db[0]["db_hostname"]
            && $db_info->master_db["db_port"] == $db_info->slave_db[0]["db_port"]
            && $db_info->master_db["db_userid"] == $db_info->slave_db[0]["db_userid"]
            && $db_info->master_db["db_password"] == $db_info->slave_db[0]["db_password"]
            && $db_info->master_db["db_database"] == $db_info->slave_db[0]["db_database"]
        ) {
            $this->slave_db[0] = & $this->master_db;
        } else {
            $this->slave_db = $db_info->slave_db;
        }
    }

    /**
     * DB Connect
     * this method is protected
     * @param array $connection
     * @return void
     */
    function __connect($connection)
    {

    }

    /**
     * If have a task after connection, add a taks in this method
     * this method is protected
     * @param resource $connection
     * @return void
     */
    function _afterConnect($connection)
    {

    }

    /**
     * DB Connect
     * this method is protected
     * @param string $type 'master' or 'slave'
     * @param int $indx number in slave dbms server list
     * @return void
     */
    function _connect($type = 'master', $indx = 0)
    {
        if ($this->isConnected($type, $indx)) {
            return;
        }

        // Ignore if no DB information exists
        if (!$this->_dbInfoExists()) {
            return;
        }

        if ($type == 'master') {
            $connection = & $this->master_db;
        } else {
            $connection = & $this->slave_db[$indx];
        }

        $result = $this->__connect($connection);
        if ($result === NULL || $result === FALSE) {
            $connection["is_connected"] = FALSE;
            return;
        }

        // Check connections
        $connection["resource"] = $result;
        $connection["is_connected"] = TRUE;

        // Save connection info for db logs
        $this->connection = ucfirst($type) . ' ' . $connection["db_hostname"];

        // regist $this->close callback
        register_shutdown_function(array($this, "close"));

        $this->_afterConnect($result);
    }

    function addQuotes($string)
    {
        $oDB = self::getInstance();
        $args = func_get_args();
        return call_user_func_array(array($oDB, 'addQuotes'), $args);
    }

    function dropTable($target_name)
    {
        $oDB = self::getInstance();
        $args = func_get_args();
        return call_user_func_array(array($oDB, 'dropTable'), $args);
    }

    function addColumn($target_name, $column_name)
    {
        $oDB = self::getInstance();
        $args = func_get_args();
        return call_user_func_array(array($oDB, 'addColumn'), $args);
    }

    function dropColumn($target_name, $column_name)
    {
        $oDB = self::getInstance();
        $args = func_get_args();
        return call_user_func_array(array($oDB, 'dropColumn'), $args);
    }

    function addIndex($table_name, $index_name, $target_columns, $is_unique = false)
    {
        $oDB = self::getInstance();
        $args = func_get_args();
        return call_user_func_array(array($oDB, 'addIndex'), $args);
    }

    function dropIndex($table_name, $index_name, $is_unique = false)
    {
        $oDB = self::getInstance();
        $args = func_get_args();
        return call_user_func_array(array($oDB, 'dropIndex'), $args);
    }

    function isTableExists($target_name)
    {
        $oDB = self::getInstance();
        $args = func_get_args();
        return call_user_func_array(array($oDB, 'isTableExists'), $args);
    }

    function isColumnExists($target_name, $column_name)
    {
        $oDB = self::getInstance();
        $args = func_get_args();
        return call_user_func_array(array($oDB, 'isColumnExists'), $args);
    }

    function isIndexExists($table_name, $index_name)
    {
        $oDB = self::getInstance();
        $args = func_get_args();
        return call_user_func_array(array($oDB, 'isIndexExists'), $args);
    }


    function isProcedureExists($procedure_name)
    {
        $oDB = self::getInstance();
        $args = func_get_args();
        return call_user_func_array(array($oDB, 'isProcedureExists'), $args);
    }

    function getNextAutoIncrement($table_name)
    {
        $oDB = self::getInstance();
        $args = func_get_args();
        return call_user_func_array(array($oDB, 'getNextAutoIncrement'), $args);
    }
}
