<?php

class Context
{
    /**
     * Conatins request parameters and environment variables
     * @var object
     */
    var $context = NULL;

    /**
     * Pattern for request vars check
     * @var array
     */
    var $patterns = array(
        '/<\?/iUsm',
        '/<\%/iUsm',
        '/<script\s*?language\s*?=\s*?("|\')?\s*?php\s*("|\')?/iUsm'
    );

    /**
     * obejct oFrontEndFileHandler()
     * @var object
     */
    public $oFrontEndFileHandler;

    /**
     * Check init
     * @var bool false if init fail
     */
    var $is_success_init = true;

    /**
     * Request method
     * @var string GET|POST|PUT|DELETE
     */
    var $request_method = 'GET';


    /**
     * variables from GET or form submit
     * @var mixed
     */
    var $get_vars = NULL;

    /**
     * Response method.If it's not set, it follows request method.
     * @var string HTML|XMLRPC
     */
    public $response_method = '';


    /**
     * returns static context object (Singleton). It's to use Context without declaration of an object
     *
     * @return object Instance
     */
    function &getInstance()
    {
        static $oInstance = null;
        if (!$oInstance) $oInstance = new Context();
        return $oInstance;
    }

    /**
     * Cunstructor
     *
     * @return void
     */
    function Context()
    {
        $this->oFrontEndFileHandler = new FrontEndFileHandler();
        $this->get_vars = new stdClass();
    }

    function init()
    {
        is_a($this, 'Context') ? $self = $this : $self = self::getInstance();

        $self->context = & $GLOBALS['__Context__'];
        $self->context->_COOKIE = $_COOKIE;
        session_start();
        if ($sess = $_POST[session_name()]) {
            session_id($sess);
        }

        $self->setRequestMethod();
        $this->_setXmlRpcArgument();
        $this->_setJSONRequestArgument();
        $self->_setRequestArgument();
        $this->_setUploadedArgument();

        $this->loadLang();
        $this->loadDBInfo();
        if (Context::get('output')) Context::setRequestMethod(strtoupper(Context::get('output')));
    }

    /**
     * Load the database information
     *
     * @return void
     */
    function loadDBInfo()
    {
        is_a($this, 'Context') ? $self = $this : $self = self::getInstance();

        $config_file = $self->getConfigFile();
        $db_info = new stdClass();
        if (is_readable($config_file)) {
            include($config_file);
        }

        // If master_db information does not exist, the config file needs to be updated
        if (!isset($db_info->master_db)) {
            $db_info->master_db = array();
            $db_info->master_db["db_type"] = $db_info->db_type;
            unset($db_info->db_type);
            $db_info->master_db["db_port"] = $db_info->db_port;
            unset($db_info->db_port);
            $db_info->master_db["db_hostname"] = $db_info->db_hostname;
            unset($db_info->db_hostname);
            $db_info->master_db["db_password"] = $db_info->db_password;
            unset($db_info->db_password);
            $db_info->master_db["db_database"] = $db_info->db_database;
            unset($db_info->db_database);
            $db_info->master_db["db_userid"] = $db_info->db_userid;
            unset($db_info->db_userid);

            $db_info->slave_db = array($db_info->master_db);
            $self->setDBInfo($db_info);
        }

        if (!$db_info->use_prepared_statements) {
            $db_info->use_prepared_statements = 'Y';
        }

        if (!$db_info->time_zone)
            $db_info->time_zone = date('O');
        $GLOBALS['_time_zone'] = $db_info->time_zone;

        if ($db_info->qmail_compatibility != 'Y')
            $db_info->qmail_compatibility = 'N';
        $GLOBALS['_qmail_compatibility'] = $db_info->qmail_compatibility;

        if (!$db_info->use_db_session)
            $db_info->use_db_session = 'N';
        if (!$db_info->use_ssl)
            $db_info->use_ssl = 'none';
        $this->set('_use_ssl', $db_info->use_ssl);

        if ($db_info->http_port)
            $self->set('_http_port', $db_info->http_port);
        if ($db_info->https_port)
            $self->set('_https_port', $db_info->https_port);

        if (!$db_info->sitelock_whitelist) {
            $db_info->sitelock_whitelist = '127.0.0.1';
        }

        if (is_string($db_info->sitelock_whitelist)) {
            $db_info->sitelock_whitelist = explode(',', $db_info->sitelock_whitelist);
        }

        $self->setDBInfo($db_info);
    }

    /**
     * Get DB's db_type
     *
     * @return string DB's db_type
     */
    function getDBType()
    {
        is_a($this, 'Context') ? $self = $this : $self = self::getInstance();
        return $self->db_info->master_db["db_type"];
    }

    /**
     * Set DB information
     *
     * @param object $db_info DB information
     * @return void
     */
    function setDBInfo($db_info)
    {
        is_a($this, 'Context') ? $self = $this : $self = self::getInstance();
        $self->db_info = $db_info;
    }

    /**
     * Get DB information
     *
     * @return object DB information
     */
    function getDBInfo()
    {
        is_a($this, 'Context') ? $self = $this : $self = self::getInstance();
        return $self->db_info;
    }

    /**
     * Get config file
     *
     * @retrun string The path of the config file that contains database settings
     */
    function getConfigFile()
    {
        return _X_PATH_ . 'config/db.config.php';
    }

    /**
     * Determine request method
     *
     * @param string $type Request method. (Optional - GET|POST|XMLRPC|JSON)
     * @return void
     */
    function setRequestMethod($type = '')
    {
        is_a($this, 'Context') ? $self = $this : $self = self::getInstance();

        $self->js_callback_func = isset($_GET['xe_js_callback']) ? $_GET['xe_js_callback'] : $_POST['xe_js_callback'];
        ($type && $self->request_method = $type) or
        (strpos($_SERVER['CONTENT_TYPE'], 'json') && $self->request_method = 'JSON') or
        (strpos($_SERVER['HTTP_ACCEPT'], 'json') && $self->request_method = 'JSON') or
        ($GLOBALS['HTTP_RAW_POST_DATA'] && $self->request_method = 'XMLRPC') or
        ($self->js_callback_func && $self->request_method = 'JS_CALLBACK') or
        ($self->request_method = $_SERVER['REQUEST_METHOD']);
    }

    /**
     * Return request method
     * @return string Request method type. (Optional - GET|POST|XMLRPC|JSON)
     */
    function getRequestMethod()
    {
        is_a($this, 'Context') ? $self =& $this : $self =& Context::getInstance();
        return $self->request_method;
    }

    /**
     * Handle request arguments for JSON
     *
     * @return void
     */
    function _setJSONRequestArgument()
    {
        if ($this->getRequestMethod() != 'JSON') {
            return;
        }

        $params = array();

        $json = json_decode($GLOBALS['HTTP_RAW_POST_DATA']);
        if(json_last_error() === JSON_ERROR_NONE){
            foreach($json as $name => $value){
                $params[$name] = $value;
            }
        }else{
            parse_str($GLOBALS['HTTP_RAW_POST_DATA'], $params);
        }

        foreach ($params as $key => $val) {
            $this->set($key, $this->_filterRequestVar($key, $val, 1), TRUE);
        }
    }


    /**
     * Handle request arguments for XML RPC
     *
     * @return void
     */
    function _setXmlRpcArgument()
    {
        if ($this->getRequestMethod() != 'XMLRPC') {
            return;
        }

        $oXml = new XmlParser();
        $xml_obj = $oXml->parse();

        $params = $xml_obj->methodcall->params;
        unset($params->node_name, $params->attrs);

        if (!count($params)) {
            return;
        }

        foreach ($params as $key => $obj) {
            $this->set($key, $this->_filterRequestVar($key, $obj->body, 0), TRUE);
        }
    }

    /**
     * handle request areguments for GET/POST
     *
     * @return void
     */
    function _setRequestArgument()
    {
        if (!count($_REQUEST)) return;

        foreach ($_REQUEST as $key => $val) {
            if ($val === '' || Context::get($key)) continue;
            $val = $this->_filterRequestVar($key, $val);
            if ($this->getRequestMethod() == 'GET' && isset($_GET[$key])) $set_to_vars = true;
            elseif ($this->getRequestMethod() == 'POST' && isset($_POST[$key])) $set_to_vars = true;
            else $set_to_vars = false;

            if ($set_to_vars) {
                $this->_recursiveCheckVar($val);
            }
            $this->set($key, $val, $set_to_vars);
        }
    }

    /**
     * Check if there exists uploaded file
     *
     * @return bool True: exists, False: otherwise
     */
    function isUploaded()
    {
        is_a($this, 'Context') ? $self = $this : $self = self::getInstance();
        return $self->is_uploaded;
    }

    /**
     * Handle uploaded file
     *
     * @return void
     */
    function _setUploadedArgument()
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST' || !$_FILES || stripos($_SERVER['CONTENT_TYPE'], 'multipart/form-data') === FALSE) {
            return;
        }

        foreach ($_FILES as $key => $val) {
            $tmp_name = $val['tmp_name'];
            if (!is_array($tmp_name)) {
                if (!$tmp_name || !is_uploaded_file($tmp_name)) {
                    continue;
                }
                $val['name'] = htmlspecialchars($val['name'], ENT_COMPAT | ENT_HTML401, 'UTF-8', FALSE);
                $this->set($key, $val, TRUE);
                $this->is_uploaded = TRUE;
            } else {
                for ($i = 0, $c = count($tmp_name); $i < $c; $i++) {
                    if ($val['size'][$i] > 0) {
                        $file['name'] = $val['name'][$i];
                        $file['type'] = $val['type'][$i];
                        $file['tmp_name'] = $val['tmp_name'][$i];
                        $file['error'] = $val['error'][$i];
                        $file['size'] = $val['size'][$i];

                        // A workaround for Firefox upload bug
                        if (preg_match('/^=\?UTF-8\?B\?(.+)\?=$/i', $file['name'], $match)) {
                            $file['name'] = base64_decode(strtr($match[1], ':', '/'));
                        }

                        $files[] = $file;
                    }
                }
                $this->set($key, $files, TRUE);
            }
        }
    }


    /**
     * Filter request variable
     *
     * @see Cast variables, such as _srl, page, and cpage, into interger
     * @param string $key Variable key
     * @param string $val Variable value
     * @param string $do_stripslashes Whether to strip slashes
     * @return mixed filtered value. Type are string or array
     */
    function _filterRequestVar($key, $val, $do_stripslashes = 1)
    {
        $isArray = TRUE;
        if (!is_array($val)) {
            $isArray = FALSE;
            $val = array($val);
        }

        foreach ($val as $k => $v) {
            if ($key === 'page' || $key === 'cpage' || substr($key, -3) === 'srl') {
                $val[$k] = !preg_match('/^[0-9,]+$/', $v) ? (int)$v : $v;
            } elseif ($key === 'search_keyword') {
                $val[$k] = htmlspecialchars($v);
            } else {
//                if ($do_stripslashes && version_compare(PHP_VERSION, '5.9.0', '<') && get_magic_quotes_gpc()) {
//                    $v = stripslashes($v);
//                }

                if (is_string($v)) $val[$k] = trim($v);
            }
        }

        if ($isArray) {
            return $val;
        } else {
            return $val[0];
        }
    }

    function _recursiveCheckVar($val)
    {
        if (is_string($val)) {
            foreach ($this->patterns as $pattern) {
                $result = preg_match($pattern, $val);
                if ($result) {
                    $this->is_success_init = FALSE;
                    return;
                }
            }
        } else if (is_array($val)) {
            foreach ($val as $val2) {
                $this->_recursiveCheckVar($val2);
            }
        }
    }

    function isSuccessInit()
    {
        return $this->is_success_init;
    }


    function loadLang(){
        @include_once _X_PATH_."/common/lang/ko.lang.php";
    }

    /**
     * Return string accoring to the inputed code
     *
     * @param string $code Language variable name
     * @return string If string for the code exists returns it, otherwise returns original code
     */
    function getLang($code)
    {
        if(!$code)
        {
            return;
        }
        if($GLOBALS['lang']->{$code})
        {
            return $GLOBALS['lang']->{$code};
        }
        return $code;
    }

    /**
     * Set data to lang variable
     *
     * @param string $code Language variable name
     * @param string $val `$code`s value
     * @return void
     */
    function setLang($code, $val)
    {
        if(!isset($GLOBALS['lang']))
        {
            $GLOBALS['lang'] = new stdClass();
        }
        $GLOBALS['lang']->{$code} = $val;
    }

    /**
     * Force to set response method
     *
     * @param string $method Response method. [HTML|XMLRPC|JSON]
     * @return void
     */
    function setResponseMethod($method = 'HTML')
    {
        is_a($this, 'Context') ? $self = $this : $self = self::getInstance();

        $methods = array('HTML' => 1, 'XMLRPC' => 1, 'JSON' => 1, 'JS_CALLBACK' => 1);
        $self->response_method = isset($methods[$method]) ? $method : 'HTML';
    }

    /**
     * Get reponse method
     *
     * @return string Response method. If it's not set, returns request method.
     */
    function getResponseMethod()
    {
        is_a($this, 'Context') ? $self = $this : $self = self::getInstance();

        if ($self->response_method) {
            return $self->response_method;
        }

        $method = $self->getRequestMethod();
        $methods = array('HTML' => 1, 'XMLRPC' => 1, 'JSON' => 1, 'JS_CALLBACK' => 1);
        return isset($methods[$method]) ? $method : 'HTML';
    }

    /**
     * Make URL with args_list upon request URL
     *
     * @param int $num_args Arguments nums
     * @param array $args_list Argument list for set url
     * @param string $domain Domain
     * @param bool $encode If TRUE, use url encode.
     * @param bool $autoEncode If TRUE, url encode automatically, detailed. Use this option, $encode value should be TRUE
     * @return string URL
     */
    function getUrl($num_args = 0, $args_list = array(), $domain = null, $encode = TRUE, $autoEncode = FALSE)
    {
        static $site_module_info = null;
        static $current_info = null;

        is_a($this, 'Context') ? $self = $this : $self = self::getInstance();


        $get_vars = array();

        // If there is no GET variables or first argument is '' to reset variables
        if (!$self->get_vars || $args_list[0] == '') {
            // rearrange args_list
            if (is_array($args_list) && $args_list[0] == '') {
                array_shift($args_list);
            }
        } else {
            // Otherwise, make GET variables into array
            $get_vars = get_object_vars($self->get_vars);
        }

        // arrange args_list
        for ($i = 0, $c = count($args_list); $i < $c; $i += 2) {
            $key = $args_list[$i];
            $val = trim($args_list[$i + 1]);

            // If value is not set, remove the key
            if (!isset($val) || !strlen($val)) {
                unset($get_vars[$key]);
                continue;
            }
            // set new variables
            $get_vars[$key] = $val;
        }


        // organize URL
        $query = '';
        if (count($get_vars) > 0) {
            if (!$query) {
                $queries = array();
                foreach ($get_vars as $key => $val) {
                    if (is_array($val) && count($val) > 0) {
                        foreach ($val as $k => $v) {
                            $queries[] = $key . '[' . $k . ']=' . urlencode($v);
                        }
                    } elseif (!is_array($val)) {
                        $queries[] = $key . '=' . urlencode($val);
                    }
                }
                if (count($queries) > 0) {
                    $query = 'index.php?' . join('&', $queries);
                }
            }
        }

        if ($domain) // if $domain is set
        {
            $query = $self->getRequestUri($domain) . $query;
        } else {
            $query = getScriptPath() . $query;

        }

        if (!$encode) {
            return $query;
        }

        if (!$autoEncode) {
            return htmlspecialchars($query, ENT_COMPAT | ENT_HTML401, 'UTF-8', FALSE);
        }

        $output = array();
        $encode_queries = array();
        $parsedUrl = parse_url($query);
        parse_str($parsedUrl['query'], $output);
        foreach ($output as $key => $value) {
            if (preg_match('/&([a-z]{2,}|#\d+);/', urldecode($value))) {
                $value = urlencode(htmlspecialchars_decode(urldecode($value)));
            }
            $encode_queries[] = $key . '=' . $value;
        }

        return htmlspecialchars($parsedUrl['path'] . '?' . join('&', $encode_queries), ENT_COMPAT | ENT_HTML401, 'UTF-8', FALSE);
    }

    /**
     * Return after removing an argument on the requested URL
     *
     * @param string $ssl_mode SSL mode
     * @param string $domain Domain
     * @retrun string converted URL
     */
    function getRequestUri($domain = null)
    {
        static $url = array();

        // Check HTTP Request
        if (!isset($_SERVER['SERVER_PROTOCOL'])) {
            return;
        }

        if ($domain) {
            $domain_key = md5($domain);
        } else {
            $domain_key = 'default';
        }

        if (isset($url[$domain_key])) {
            return $url[$domain_key];
        }

        if ($domain) {
            $target_url = trim($domain);
            if (substr_compare($target_url, '/', -1) !== 0) {
                $target_url .= '/';
            }
        } else {
            $target_url = $_SERVER['HTTP_HOST'] . getScriptPath();
        }

        $url_info = parse_url('http://' . $target_url);

        $url[$domain_key] = sprintf('%s://%s%s%s', $url_info['scheme'], $url_info['host'], $url_info['port'] && $url_info['port'] != 80 ? ':' . $url_info['port'] : '', $url_info['path']);

        return $url[$domain_key];
    }

    /**
     * Return key's value
     *
     * @param string $key Key
     * @return string Key
     */
    function get($key)
    {
        is_a($this, 'Context') ? $self =& $this : $self =& Context::getInstance();

        if (!isset($self->context->{$key})) return null;
        return $self->context->{$key};
    }


    /**
     * Set a context value with a key
     *
     * @param string $key Key
     * @param string $val Value
     * @param mixed $is_get_vars If not false, Set to get vars.
     * @return void
     */
    function set($key, $val, $is_get_vars = 0)
    {
        is_a($this, 'Context') ? $self =& $this : $self =& Context::getInstance();

        $self->context->{$key} = $val;
        if ($is_get_vars === false) return;
        if ($val === NULL || $val === '') {
            unset($self->get_vars->{$key});
            return;
        }
        if ($is_get_vars || $self->get_vars->{$key}) $self->get_vars->{$key} = $val;
    }


    /**
     * Get one more vars in object vars with given arguments(key1, key2, key3,...)
     *
     * @return object
     */
    function gets()
    {
        $num_args = func_num_args();
        if ($num_args < 1) return;
        is_a($this, 'Context') ? $self =& $this : $self =& Context::getInstance();

        $output = new stdClass();
        $args_list = func_get_args();
        foreach ($args_list as $v) {
            $output->{$v} = $self->get($v);
        }
        return $output;
    }

    /**
     * Return all data
     *
     * @return object All data
     */
    function getAll()
    {
        is_a($this, 'Context') ? $self =& $this : $self =& Context::getInstance();
        return $self->context;
    }

    /**
     * Load front end file
     *
     * @param array $args array
     * case js :
     *        $args[0]: file name,
     *        $args[1]: type (head | body),
     *        $args[2]: target IE,
     *        $args[3]: index
     * case css :
     *        $args[0]: file name,
     *        $args[1]: media,
     *        $args[2]: target IE,
     *        $args[3]: index
     *
     */
    function loadFile($args)
    {
        is_a($this, 'Context') ? $self = $this : $self = self::getInstance();
        $self->oFrontEndFileHandler->loadFile($args);
    }

    /**
     * Unload front end file
     *
     * @param string $file File name with path
     * @param string $targetIe Target IE
     * @param string $media Media query
     * @return void
     */
    function unloadFile($file, $targetIe = '', $media = 'all')
    {
        is_a($this, 'Context') ? $self = $this : $self = self::getInstance();
        $self->oFrontEndFileHandler->unloadFile($file, $targetIe, $media);
    }

    /**
     * Unload front end file all
     *
     * @param string $type Unload target (optional - all|css|js)
     * @return void
     */
    function unloadAllFiles($type = 'all')
    {
        is_a($this, 'Context') ? $self = $this : $self = self::getInstance();
        $self->oFrontEndFileHandler->unloadAllFiles($type);
    }

    /**
     * Add the js file
     *
     * @deprecated
     * @param string $file File name with path
     * @param string $optimized optimized (That seems to not use)
     * @param string $targetie target IE
     * @param string $index index
     * @param string $type Added position. (head:<head>..</head>, body:<body>..</body>)
     * @return void
     */
    function addJsFile($file, $targetie = '', $index = 0, $type = 'head')
    {
        is_a($this, 'Context') ? $self = $this : $self = self::getInstance();
        $self->oFrontEndFileHandler->loadFile(array($file, $type, $targetie, $index));
    }

    /**
     * Returns the list of javascripts that matches the given type.
     *
     * @param string $type Added position. (head:<head>..</head>, body:<body>..</body>)
     * @return array Returns javascript file list. Array contains file, targetie.
     */
    function getJsFile($type = 'head')
    {
        is_a($this, 'Context') ? $self = $this : $self = self::getInstance();
        return $self->oFrontEndFileHandler->getJsFileList($type);
    }

    /**
     * Add CSS file
     *
     * @deprecated
     * @param string $file File name with path
     * @param string $optimized optimized (That seems to not use)
     * @param string $media Media query
     * @param string $targetie target IE
     * @param string $index index
     * @return void
     *
     */
    function addCSSFile($file, $optimized = FALSE, $media = 'all', $targetie = '', $index = 0)
    {
        is_a($this, 'Context') ? $self = $this : $self = self::getInstance();
        $self->oFrontEndFileHandler->loadFile(array($file, $media, $targetie, $index));
    }

    /**
     * Remove css file
     *
     * @deprecated
     * @param string $file File name with path
     * @param string $optimized optimized (That seems to not use)
     * @param string $media Media query
     * @param string $targetie target IE
     * @return void
     */
    function unloadCSSFile($file, $optimized = FALSE, $media = 'all', $targetie = '')
    {
        is_a($this, 'Context') ? $self = $this : $self = self::getInstance();
        $self->oFrontEndFileHandler->unloadFile($file, $targetie, $media);
    }

    /**
     * Unload all css files
     *
     * @return void
     */
    function unloadAllCSSFiles()
    {
        is_a($this, 'Context') ? $self = $this : $self = self::getInstance();
        $self->oFrontEndFileHandler->unloadAllFiles('css');
    }

    /**
     * Return a list of css files
     *
     * @return array Returns css file list. Array contains file, media, targetie.
     */
    function getCSSFile()
    {
        is_a($this, 'Context') ? $self = $this : $self = self::getInstance();
        return $self->oFrontEndFileHandler->getCssFileList();
    }

    /**
     * Add html code before </head>
     *
     * @param string $header add html code before </head>.
     * @return void
     */
    function addHtmlHeader($header)
    {
        is_a($this, 'Context') ? $self = $this : $self = self::getInstance();
        $self->oFrontEndFileHandler->addHtmlHeader($header);
    }

    function clearHtmlHeader()
    {
        is_a($this, 'Context') ? $self = $this : $self = self::getInstance();
        $self->oFrontEndFileHandler->clearHtmlHeader();
    }

    /**
     * Returns added html code by addHtmlHeader()
     *
     * @return string Added html code before </head>
     */
    function getHtmlHeader()
    {
        is_a($this, 'Context') ? $self = $this : $self = self::getInstance();
        return $self->oFrontEndFileHandler->getHtmlHeader();
    }

    /**
     * Add css class to Html Body
     *
     * @param string $class_name class name
     */
    function addBodyClass($class_name)
    {
        is_a($this, 'Context') ? $self = $this : $self = self::getInstance();
        $self->oFrontEndFileHandler->addBodyClass($class_name);
    }

    /**
     * Return css class to Html Body
     *
     * @return string Return class to html body
     */
    function getBodyClass()
    {
        is_a($this, 'Context') ? $self = $this : $self = self::getInstance();
        return $self->oFrontEndFileHandler->getBodyClass();
    }

    /**
     * Add html code after <body>
     *
     * @param string $header Add html code after <body>
     */
    function addBodyHeader($header)
    {
        is_a($this, 'Context') ? $self = $this : $self = self::getInstance();
        $self->oFrontEndFileHandler->addBodyHeader($header);
    }

    /**
     * Returns added html code by addBodyHeader()
     *
     * @return string Added html code after <body>
     */
    function getBodyHeader()
    {
        is_a($this, 'Context') ? $self = $this : $self = self::getInstance();
        return $self->oFrontEndFileHandler->getBodyHeader();
    }

    /**
     * Add html code before </body>
     *
     * @param string $footer Add html code before </body>
     */
    function addHtmlFooter($footer)
    {
        is_a($this, 'Context') ? $self = $this : $self = self::getInstance();
        $self->oFrontEndFileHandler->addHtmlFooter($footer);
    }

    /**
     * Returns added html code by addHtmlHeader()
     *
     * @return string Added html code before </body>
     */
    function getHtmlFooter()
    {
        is_a($this, 'Context') ? $self = $this : $self = self::getInstance();
        return $self->oFrontEndFileHandler->getHtmlFooter();
    }

    function close()
    {
    }
}

?>
