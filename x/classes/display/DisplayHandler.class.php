<?php

class DisplayHandler
{
    var $content_size = 0; // /< The size of displaying contents
    var $gz_enabled = FALSE; // / <a flog variable whether to call contents after compressing by gzip
    var $handler = null;

    /**
     * print either html or xml content given oModule object
     * @remark addon execution and the trigger execution are included within this method, which might create inflexibility for the fine grained caching
     * @param ModuleObject $oModule the module object
     * @return void
     */
    function printContent(&$oModule)
    {
        // Extract contents to display by the request method
        if (Context::get('xeVirtualRequestMethod') == 'xml') {
            require_once(_X_PATH_ . "classes/display/VirtualXMLDisplayHandler.php");
            $handler = new VirtualXMLDisplayHandler();
        } else if (Context::getRequestMethod() == 'XMLRPC') {
            require_once(_X_PATH_ . "classes/display/XMLDisplayHandler.php");
            $handler = new XMLDisplayHandler();
            if (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== FALSE) {
                $this->gz_enabled = FALSE;
            }
        } else if (Context::getRequestMethod() == 'JSON') {
            require_once(_X_PATH_ . "classes/display/JSONDisplayHandler.php");
            $handler = new JSONDisplayHandler();
        } else if (Context::getRequestMethod() == 'JS_CALLBACK') {
            require_once(_X_PATH_ . "classes/display/JSCallbackDisplayHandler.php");
            $handler = new JSCallbackDisplayHandler();
        } else {
            require_once(_X_PATH_ . "classes/display/HTMLDisplayHandler.php");
            $handler = new HTMLDisplayHandler();
        }
        $output = $handler->toDoc($oModule);

        if(method_exists($handler, "prepareToPrint"))
        {
            $handler->prepareToPrint($output);
        }

        // header output
        if ($this->gz_enabled) {
            header("Content-Encoding: gzip");
        }

        $httpStatusCode = $oModule->getHttpStatusCode();
        if ($httpStatusCode && $httpStatusCode != 200) {
            $this->_printHttpStatusCode($httpStatusCode);
        } else {
            if (Context::getResponseMethod() == 'JSON' || Context::getResponseMethod() == 'JS_CALLBACK') {
                $this->_printJSONHeader();
            } else if (Context::getResponseMethod() != 'HTML') {
                $this->_printXMLHeader();
            } else {
                $this->_printHTMLHeader();
            }
        }

        // debugOutput output
        $this->content_size = strlen($output);

        // results directly output
        if ($this->gz_enabled) {
            print ob_gzhandler($output, 5);
        } else {
            print $output;
        }
    }

    /**
     * print a HTTP HEADER for XML, which is encoded in UTF-8
     * @return void
     */
    function _printXMLHeader()
    {
        header("Content-Type: text/xml; charset=UTF-8");
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
    }

    /**
     * print a HTTP HEADER for HTML, which is encoded in UTF-8
     * @return void
     */
    function _printHTMLHeader()
    {
        header("Content-Type: text/html; charset=UTF-8");
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
    }

    /**
     * print a HTTP HEADER for JSON, which is encoded in UTF-8
     * @return void
     */
    function _printJSONHeader()
    {
        header("Content-Type: application/json; charset=UTF-8");
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
    }

    /**
     * print a HTTP HEADER for HTML, which is encoded in UTF-8
     * @return void
     */
    function _printHttpStatusCode($code)
    {
        $statusMessage = Context::get('http_status_message');
        header("HTTP/1.0 $code $statusMessage");
    }
}

?>
