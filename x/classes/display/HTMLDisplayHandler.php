<?php
class HTMLDisplayHandler
{

    /**
     * Produce HTML compliant content given a module object.\n
     * @param ModuleObject $oModule the module object
     * @return string compiled template string
     */
    function toDoc(&$oModule)
    {
        $oTemplate = TemplateHandler::getInstance();

        $template_path = '';
        $tpl_file = '';
        if ($oModule != null && ($oModule instanceof ModuleObject)) {
            $template_path = $oModule->getTemplatePath();
            $tpl_file = $oModule->getTemplateFile();
        }

        if (!$template_path || !$tpl_file) {
            $template_path = 'common/tpl/';
            $tpl_file = 'default_layout.html';
        }

        $output = $oTemplate->compile($template_path, $tpl_file);
        return $output;
    }

    /**
     * when display mode is HTML, prepare code before print.
     * @param string $output compiled template string
     * @return void
     */
    function prepareToPrint(&$output)
    {
        if (Context::getResponseMethod() != 'HTML') {
            return;
        }

        // move <style ..></style> in body to the header
        $output = preg_replace_callback('!<style(.*?)>(.*?)<\/style>!is', array($this, '_moveStyleToHeader'), $output);

        // move <link ..></link> in body to the header
        $output = preg_replace_callback('!<link(.*?)/>!is', array($this, '_moveLinkToHeader'), $output);

        // move <meta ../> in body to the header
        $output = preg_replace_callback('!<meta(.*?)(?:\/|)>!is', array($this, '_moveMetaToHeader'), $output);

        // change a meta fine(widget often put the tag like <!--Meta:path--> to the content because of caching)
        $output = preg_replace_callback('/<!--(#)?Meta:([a-z0-9\_\/\.\@]+)-->/is', array($this, '_transMeta'), $output);

        // prevent the 2nd request due to url(none) of the background-image
        $output = preg_replace('/url\((["\']?)none(["\']?)\)/is', 'none', $output);

        $this->_loadJSCSS();
    }

    /**
     * when display mode is HTML, prepare code before print about <input> tag value.
     * @param array $match input value.
     * @return string input value.
     */
    function _preserveValue($match)
    {
        $INPUT_ERROR = Context::get('INPUT_ERROR');

        $str = $match[1] . $match[2] . ' name="' . $match[3] . '"' . $match[4];

        // get type
        $type = 'text';
        if (preg_match('/\stype="([a-z]+)"/i', $str, $m)) {
            $type = strtolower($m[1]);
        }

        switch ($type) {
            case 'text':
            case 'hidden':
            case 'email':
            case 'search':
            case 'tel':
            case 'url':
            case 'email':
            case 'datetime':
            case 'date':
            case 'month':
            case 'week':
            case 'time':
            case 'datetime-local':
            case 'number':
            case 'range':
            case 'color':
                $str = preg_replace('@\svalue="[^"]*?"@', ' ', $str) . ' value="' . htmlspecialchars($INPUT_ERROR[$match[3]], ENT_COMPAT | ENT_HTML401, 'UTF-8', false) . '"';
                break;
            case 'password':
                $str = preg_replace('@\svalue="[^"]*?"@', ' ', $str);
                break;
            case 'radio':
            case 'checkbox':
                $str = preg_replace('@\schecked(="[^"]*?")?@', ' ', $str);
                if (@preg_match('@\s(?i:value)="' . $INPUT_ERROR[$match[3]] . '"@', $str)) {
                    $str .= ' checked="checked"';
                }
                break;
        }

        return $str . ' />';
    }

    /**
     * when display mode is HTML, prepare code before print about <select> tag value.
     * @param array $matches select tag.
     * @return string select tag.
     */
    function _preserveSelectValue($matches)
    {
        $INPUT_ERROR = Context::get('INPUT_ERROR');
        preg_replace('@\sselected(="[^"]*?")?@', ' ', $matches[0]);
        preg_match('@<select.*?>@is', $matches[0], $mm);

        preg_match_all('@<option[^>]*\svalue="([^"]*)".+</option>@isU', $matches[0], $m);

        $key = array_search($INPUT_ERROR[$matches[1]], $m[1]);
        if ($key === FALSE) {
            return $matches[0];
        }

        $m[0][$key] = preg_replace('@(\svalue=".*?")@is', '$1 selected="selected"', $m[0][$key]);

        return $mm[0] . implode('', $m[0]) . '</select>';
    }

    /**
     * when display mode is HTML, prepare code before print about <textarea> tag value.
     * @param array $matches textarea tag information.
     * @return string textarea tag
     */
    function _preserveTextAreaValue($matches)
    {
        $INPUT_ERROR = Context::get('INPUT_ERROR');
        preg_match('@<textarea.*?>@is', $matches[0], $mm);
        return $mm[0] . $INPUT_ERROR[$matches[1]] . '</textarea>';
    }

    /**
     * add html style code extracted from html body to Context, which will be
     * printed inside <header></header> later.
     * @param array $matches
     * @return void
     */
    function _moveStyleToHeader($matches)
    {
        if (isset($matches[1]) && stristr($matches[1], 'scoped')) {
            return $matches[0];
        }
        Context::addHtmlHeader($matches[0]);
    }

    /**
     * add html link code extracted from html body to Context, which will be
     * printed inside <header></header> later.
     * @param array $matches
     * @return void
     */
    function _moveLinkToHeader($matches)
    {
        Context::addHtmlHeader($matches[0]);
    }

    /**
     * add meta code extracted from html body to Context, which will be
     * printed inside <header></header> later.
     * @param array $matches
     * @return void
     */
    function _moveMetaToHeader($matches)
    {
        Context::addHtmlHeader($matches[0]);
    }

    /**
     * add given .css or .js file names in widget code to Context
     * @param array $matches
     * @return void
     */
    function _transMeta($matches)
    {
        if ($matches[1]) {
            return '';
        }
        Context::loadFile($matches[2]);
    }

    /**
     * import basic .js files.
     * @return void
     */
    function _loadJSCSS()
    {
        $oContext = Context::getInstance();
        $oContext->loadFile(array('./common/js/libs/require/2.1.11/require.min.js', '', '', -1000000), true);
        $oContext->loadFile(array('./common/js/bootstrap.js', '', '', -1000000), true);
    }


}