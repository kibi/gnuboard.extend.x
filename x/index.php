<?php
define("_X_STANDALONE_", $_SERVER['SCRIPT_FILENAME'] === __FILE__);

if (_X_STANDALONE_) {
    require_once '../common.php';
}

define('__X__', true);
require_once dirname(__FILE__) . '/config/config.inc.php';

$oContext = & Context::getInstance();
$oContext->init();

if (_X_STANDALONE_) {
    if ($oContext->getResponseMethod() == 'HTML') {
        ExtendX::init();
        ExtendX::start();
    }

    $oModuleHandler = new ModuleHandler();
    try {
        if ($oModuleHandler->init()) {
            $oModule = & $oModuleHandler->procModule();
            $oModuleHandler->displayContent($oModule);
        }
    } catch (Exception $e) {
        echo $e->getMessage();
    }

    if ($oContext->getResponseMethod() == 'HTML') {
        ExtendX::close();
    }
}